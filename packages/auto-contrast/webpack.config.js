/**
 * File: /webpack.config.js
 * Project: @native-theme-ui/core
 * File Created: 29-06-2022 07:38:31
 * Author: Clay Risser
 * -----
 * Last Modified: 09-11-2022 11:25:55
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const createExpoWebpackConfigAsync = require('@expo/webpack-config');
const transpileModules = require('./transpileModules');
const pkg = require('./package.json');

module.exports = (config, argv) => {
  return createExpoWebpackConfigAsync(
    {
      ...config,
      babel: {
        dangerouslyAddModulePathsToTranspile: [...transpileModules, ...pkg.transpileModules],
      },
    },
    argv,
  );
};
