/**
 * File: /src/hooks/useBackgroundColor.ts
 * Project: auto-contrast
 * File Created: 02-03-2022 03:20:14
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 10:02:24
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { useContext } from 'react';
import BackgroundColorContext from '../contexts/backgroundColor';
import { useThemeLookup } from '@native-theme-ui/dripsy';

export function useBackgroundColor(themeKey?: string): string | undefined {
  const backgroundColor = useContext(BackgroundColorContext);
  const themeLookup = useThemeLookup(themeKey || backgroundColor.themeKey);
  return themeLookup<string>('backgroundColor', backgroundColor.color)?.toString();
}
