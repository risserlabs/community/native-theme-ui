/**
 * File: /src/hooks/useColor.ts
 * Project: auto-contrast
 * File Created: 02-03-2022 02:57:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:42:03
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import camelCase from 'lodash.camelcase';
import colorString from 'color-string';
import startCase from 'lodash.startcase';
import type { StyleProp } from 'react-native';
import { DripsyFinalTheme } from 'dripsy';
import { Generator as ColorGenerator, Generator } from 'contrast-color-generator';
import { SxProp, Sx, useDripsyTheme, useThemeLookup, useCalculatedSx, lookupStyle } from '@native-theme-ui/dripsy';
import { score, hex } from 'wcag-contrast';
import { useContext } from 'react';
import BackgroundColorContext from '../contexts/backgroundColor';
import { AutoContrast, AutoContrastLevel, AutoContrastConfig } from '../types';

export function useColor(
  props: {
    autoContrast?: AutoContrast;
    style?: StyleProp<unknown>;
    sx?: SxProp;
  } & unknown,
  themeKey?: string,
  ignoreContext = false,
  backgroundColor?: string | unknown,
  sx?: Sx,
) {
  if (!sx) sx = useCalculatedSx(props.sx || {});
  const themeLookup = useThemeLookup(themeKey);
  const { theme } = useDripsyTheme() as {
    theme: DripsyFinalTheme & {
      autoContrast?: AutoContrast;
    };
  };
  const autoContrastConfig = getAutoContrastConfig(props.autoContrast, theme.autoContrast);
  const backgroundColorContext = useContext(BackgroundColorContext);
  let isBackgroundColorFromStyle = false;
  if (!backgroundColor) {
    backgroundColor = lookupStyle(props.style, 'backgroundColor');
    if (backgroundColor) {
      isBackgroundColorFromStyle = true;
    } else {
      backgroundColor = sx.backgroundColor || sx.bg;
    }
  }
  let isBackgroundColorFromContext = false;
  if ((!backgroundColor || backgroundColor === 'transparent') && !ignoreContext) {
    backgroundColor = backgroundColorContext?.color;
    if (backgroundColor) isBackgroundColorFromContext = true;
  }
  if (!isBackgroundColorFromStyle) {
    backgroundColor =
      themeLookup<string>(
        'backgroundColor',
        backgroundColor,
        (isBackgroundColorFromContext && backgroundColorContext.themeKey) || themeKey,
      ) || (backgroundColor as string);
  }
  let color: string | undefined = lookupStyle(props.style, 'color');
  let isColorFromStyle = false;
  if (color) {
    isColorFromStyle = true;
  } else {
    color = sx?.color as string;
  }
  if (
    !color &&
    !isBackgroundColorFromStyle &&
    typeof backgroundColor === 'string' &&
    backgroundColor !== 'transparent' &&
    backgroundColor.length > 0 &&
    backgroundColor[0] !== '#' &&
    autoContrastConfig
  ) {
    const colorKey = `text${startCase(camelCase(backgroundColor)).replace(/ /g, '')}`;
    color = themeLookup<string>('color', colorKey);
    if (color === colorKey) color = undefined;
  } else if (color && !isColorFromStyle) {
    color = themeLookup<string>('color', color) || (color as string);
  }
  if (!color) color = '#000000';
  if (autoContrastConfig === false) return color;
  return autoContrast(
    (backgroundColor as string) || '#FFFFFF',
    color,
    autoContrastConfig.level,
    autoContrastConfig.minimumRatio,
    autoContrastConfig.hue,
    autoContrastConfig.brighterFirst,
  );
}

export function getAutoContrastConfig(
  autoContrast?: AutoContrast,
  autoContrastDefault?: AutoContrast,
): AutoContrastConfig | false {
  if (autoContrast === 'true') {
    autoContrast = true;
  } else if (autoContrast === 'false') {
    autoContrast = false;
  }
  let autoContrastConfig: Partial<AutoContrastConfig> = {};
  let autoContrastDefaultConfig: AutoContrastConfig = {
    level: false,
  };
  if (typeof autoContrast === 'object') {
    autoContrastConfig = {
      ...autoContrastConfig,
      ...autoContrast,
    };
  } else if (typeof autoContrast === 'string' || typeof autoContrast === 'boolean') {
    autoContrastConfig.level = autoContrast;
  }
  if (typeof autoContrastDefault === 'object') {
    autoContrastConfig = {
      ...autoContrastDefaultConfig,
      ...autoContrastDefault,
    };
  } else if (typeof autoContrastDefault === 'string' || typeof autoContrastDefault === 'boolean') {
    autoContrastDefaultConfig.level = autoContrastDefault;
  }
  const mergedAutoContrastConfig = {
    ...autoContrastDefaultConfig,
    ...autoContrastConfig,
  };
  if (mergedAutoContrastConfig.level === false) return false;
  return mergedAutoContrastConfig;
}

export function contrast(color: string, minimumRatio = 21, hue = 180, brighterFirst = true): string {
  const generator = new ColorGenerator(hue, {
    minimumRatio,
    searchPrior: brighterFirst ? Generator.BRIGHTER_FIRST : Generator.DARKER_FIRST,
  });
  return generator.generate(toHex(color)).hexStr;
}

export function autoContrast(
  color: string | null,
  originalColor: string,
  level: AutoContrastLevel = false,
  minimumRatio = 10,
  hue?: number,
  brighterFirst?: boolean,
): string {
  if (!level || !color) return originalColor;
  const scoreResult = score(hex(toHex(color), toHex(originalColor)));
  if (scoreResult !== 'Fail' && scoreResult.length > (level === true ? 2 : level.length)) {
    return originalColor;
  }
  try {
    return contrast(color, minimumRatio, hue, brighterFirst);
  } catch (err) {
    const error = err as Error;
    if (error?.message === 'No color exist which satisfies a requirement.') {
      if (!minimumRatio) return originalColor;
      return autoContrast(color, originalColor, level, minimumRatio - 1, hue, brighterFirst);
    }
    throw err;
  }
}

export function toHex(color: string): string {
  return colorString.to.hex(colorString.get(color)?.value || [0, 0, 0]).substring(0, 7);
}
