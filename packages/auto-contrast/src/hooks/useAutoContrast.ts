/**
 * File: /src/hooks/useAutoContrast.ts
 * Project: @native-theme-ui/auto-contrast
 * File Created: 13-06-2022 01:12:41
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 10:10:06
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { StyleProp } from 'react-native';
import { useCalculatedSx, Sx, SxProp } from '@native-theme-ui/dripsy';
import { useColor } from './useColor';
import { AutoContrast } from '../types';

export function useAutoContrast(
  props: {
    autoContrast?: AutoContrast;
    style?: StyleProp<unknown>;
    sx?: SxProp;
    themeKey?: string;
    variant?: any;
  } & unknown,
  defaultStyle?: Sx,
  themeKey?: string,
  ignoreContext = false,
  backgroundColor?: string | unknown,
): Omit<Sx, 'color'> & { color: string } {
  const sx = useCalculatedSx(props.sx || {});
  const color = useColor(props, props.themeKey || themeKey, ignoreContext, backgroundColor, {
    ...defaultStyle,
    ...sx,
  });
  return { ...sx, color };
}
