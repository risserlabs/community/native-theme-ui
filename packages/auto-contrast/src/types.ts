/**
 * File: /src/types.ts
 * Project: auto-contrast
 * File Created: 02-03-2022 02:58:25
 * Author: Clay Risser
 * -----
 * Last Modified: 03-07-2022 08:55:49
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export type AutoContrastLevel = boolean | 'A' | 'AA' | 'AAA' | 'false' | 'true';

export interface AutoContrastConfig {
  level: AutoContrastLevel;
  minimumRatio?: number;
  hue?: number;
  brighterFirst?: boolean;
}

export type AutoContrast = AutoContrastLevel | AutoContrastConfig;
