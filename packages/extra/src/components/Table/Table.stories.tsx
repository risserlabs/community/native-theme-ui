/**
 * File: /src/components/Table/Table.stories.tsx
 * Project: @native-theme-ui/extra
 * File Created: 13-12-2022 10:39:17
 * Author: Clay Risser
 * -----
 * Last Modified: 31-12-2022 14:54:27
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { action } from '@storybook/addon-actions';
import { Row } from '../Row';
import { Column } from '../Column';
import { autoContrastArgType, createArgsStory, createSxArgs, sxArgTypes } from '../../../storybook';
import { Table } from './index';

export default {
  title: 'components/table/Table',
  component: Table,
  parameters: {
    status: { type: 'beta' },
  },
};

export const select = createArgsStory(Table, { onValueChange: action('onValueChange') }, [
  <Row
    sx={{ borderWidth: 1, margin: 1, padding: 1 }}
    data={[
      ['1', '2', '3', '4'],
      ['a', 'b', 'c', 'd'],
      ['5', '6', '7', '8'],
      ['e', 'f', 'g', 'h'],
    ]}
  />,
  <Column
    sx={{ borderWidth: 1, margin: 1, padding: 1 }}
    data={[
      ['1', '2', '3', '4'],
      ['ajith', 'kumar', 'hello'],
      ['e', 'f', 'g', 'h'],
    ]}
  />,
]);
select.args = {
  ...createSxArgs(),
};
select.argTypes = {
  selectedValue: { control: { type: 'text' } },
  autoContrast: {
    options: ['A', 'AA', 'AAA', 'false'],
    control: { type: 'select' },
    ...autoContrastArgType,
    ...sxArgTypes,
  },
};
