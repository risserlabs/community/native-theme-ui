/**
 * File: /src/components/Rows/Rows.stories.tsx
 * Project: @native-theme-ui/extra
 * File Created: 29-12-2022 12:36:53
 * Author: Ajith Kumar
 * -----
 * Last Modified: 31-12-2022 11:44:08
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Rows } from './index';
import { autoContrastArgType, createSxArgs, sxArgTypes } from '../../../storybook';
import { Table } from '../Table';
import { Column } from '../Column';
import { Cell } from '../Cell';

export default {
  title: 'components/table/Rows',
  component: Rows,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => (
  <Table>
    <Column
      sx={{ borderWidth: 1, bg: 'red', margin: 1 }}
      data={[
        ['1', '2', '3', '4'],
        ['ajith', 'kumar'],
        ['may', 'I', 'come', 'in'],
      ]}
    />
    <Rows
      sx={{ borderWidth: 1 }}
      data={[
        ['a', 'b', 'c', 'd'],
        ['this', 'is', 'row'],
      ]}
    >
      <Cell data="hello world" />
      <Column
        sx={{ borderWidth: 1 }}
        data={[
          ['a', 'b', 'c', 'd'],
          ['this', 'is', 'row'],
        ]}
      />
    </Rows>
    <Rows
      sx={{ borderWidth: 1 }}
      data={[
        ['a', 'b', 'c', 'd'],
        ['this', 'is', 'row'],
      ]}
    />
  </Table>
);
main.args = {
  ...createSxArgs,
};
main.argTypes = {
  selectedValue: { control: { type: 'text' } },
  autoContrast: {
    options: ['A', 'AA', 'AAA', 'false'],
    control: { type: 'select' },
    ...autoContrastArgType,
    ...sxArgTypes,
  },
};
