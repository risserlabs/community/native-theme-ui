/**
 * File: /src/components/TableWrap/TableWrapper.stories.tsx
 * Project: @native-theme-ui/extra
 * File Created: 04-01-2023 15:49:08
 * Author: Ajith Kumar
 * -----
 * Last Modified: 04-01-2023 15:59:07
 * Modified By: Ajith Kumar
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2023
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Table } from '../Table';
import { Column } from '../Column';
import { Row } from '../Row';
import { TableWrapper } from './index';

export default {
  title: 'components/table/TableWrapper',
  component: TableWrapper,
  parameters: {
    status: { type: 'beta' },
  },
};

export const main = () => (
  <TableWrapper sx={{ bg: 'red' }}>
    <Table borderStyle={{ borderWidth: 1, padding: 2 }}>
      <Column
        sx={{ borderWidth: 1 }}
        data={[
          ['a', 'b', 'c', 'd'],
          ['this', 'is', 'row'],
        ]}
      />
      <Row
        sx={{ borderWidth: 1 }}
        data={[
          ['a', 'b', 'c', 'd'],
          ['this', 'is', 'row'],
        ]}
      />
    </Table>
  </TableWrapper>
);
