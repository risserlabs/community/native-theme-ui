/**
 * File: /src/components/Cell/index.tsx
 * Project: @native-theme-ui/extra
 * File Created: 29-12-2022 14:35:08
 * Author: Ajith Kumar
 * -----
 * Last Modified: 04-01-2023 01:57:18
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Cell as RNCell, CellProps as RNCellProps } from 'react-native-table-component';
import { styled, DripsyStyledProps } from '@native-theme-ui/dripsy';

export type CellProps = DripsyStyledProps<RNCellProps>;

export const Cell = styled(RNCell, {
  themeKey: 'cell',
  defaultVariant: 'primary',
})();
