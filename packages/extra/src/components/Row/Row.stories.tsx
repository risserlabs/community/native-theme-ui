/**
 * File: /src/components/Row/Row.stories.tsx
 * Project: @native-theme-ui/extra
 * File Created: 29-12-2022 10:36:43
 * Author: Ajith Kumar
 * -----
<<<<<<< HEAD
 * Last Modified: 31-12-2022 12:08:36
 * Modified By: Ajith Kumar
=======
 * Last Modified: 31-12-2022 11:50:06
 * Modified By: K S R P BHUSHAN
>>>>>>> f57879cfe68c258b9eb4f78bd88e9df26efe4953
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Row } from './index';
import { autoContrastArgType, createSxArgs, sxArgTypes } from '../../../storybook';
import { Table } from '../Table';

export default {
  title: 'components/table/Row',
  component: Row,
  parameters: { status: { type: 'beta' } },
};

export const main = () => (
  <Table>
    <Row
      sx={{ borderWidth: 1, padding: 2, justifyContent: 'space-between' }}
      data={[
        ['this', 'is', 'from', 'row'],
        ['this', 'was', 'created', 'by', 'ajith'],
      ]}
    />
  </Table>
);

main.args = {
  ...createSxArgs,
};
main.argTypes = {
  selectedValue: { control: { type: 'text' } },
  autoContrast: {
    options: ['A', 'AA', 'AAA', 'false'],
    control: { type: 'color' },
    ...autoContrastArgType,
    ...sxArgTypes,
  },
};
