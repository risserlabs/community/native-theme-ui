/**
 * File: /src/components/SelectButton/context.ts
 * Project: @native-theme-ui/extra
 * File Created: 13-12-2022 11:33:18
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 13:02:33
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SxProp } from '@native-theme-ui/dripsy';
import { createContext, Dispatch, SetStateAction } from 'react';

export const SelectButtonContext = createContext<SelectButtonContextValue>({ values: [] });

interface SelectButtonContextValue {
  buttonSx?: SxProp;
  selectedIndex?: number;
  selectedSx?: SxProp;
  setSelectedIndex?: Dispatch<SetStateAction<number | undefined>>;
  setValues?: Dispatch<SetStateAction<(string | number)[]>>;
  values: (string | number)[];
}
