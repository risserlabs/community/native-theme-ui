/**
 * File: /src/components/SelectButton/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 13:02:04
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, useState } from 'react';
import { Box, BoxProps } from '@native-theme-ui/core';
import { OptionButton, OptionButtonProps } from './OptionButton';
import { SxProp, Sx, useCalculatedSx } from '@native-theme-ui/dripsy';
import { useEffect } from 'react';
import { SelectButtonContext } from './context';

export type SelectButtonProps = BoxProps & {
  buttonSx?: SxProp;
  onValueChange?: (value: number | string, index: number) => unknown;
  selectedSx?: SxProp;
  selectedValue?: number | string;
};

export const SelectButton: FC<SelectButtonProps> & { OptionButton: typeof OptionButton } = (
  props: SelectButtonProps,
) => {
  const { selectedValue } = props;
  const [values, setValues] = useState<(string | number)[]>([]);
  const [selectedIndex, setSelectedIndex] = useState<number>();
  const buttonSx = useCalculatedSx(props.buttonSx);
  const selectedSx = useCalculatedSx(props.selectedSx);
  const sx = useCalculatedSx(props.sx);
  const clonedProps = { ...props };
  delete clonedProps.buttonSx;
  delete clonedProps.selectedSx;

  useEffect(() => {
    if (props.onValueChange && typeof selectedIndex === 'number' && values.length && selectedIndex < values.length) {
      props.onValueChange(values[selectedIndex], selectedIndex);
    }
  }, [selectedIndex]);

  useEffect(() => {
    if (typeof selectedValue === 'undefined') return;
    for (const [index, value] of values.entries()) {
      if (value === selectedValue) {
        setSelectedIndex(index);
        return;
      }
    }
    setSelectedIndex(undefined);
  }, [selectedValue]);

  return (
    <SelectButtonContext.Provider
      value={{
        selectedIndex,
        setSelectedIndex,
        setValues,
        values,
        buttonSx: {
          ...defaultButtonSx,
          ...buttonSx,
        },
        selectedSx: {
          ...defaultSelectedSx,
          ...selectedSx,
        },
      }}
    >
      <Box
        {...clonedProps}
        sx={{
          ...defaultSx,
          ...sx,
        }}
      />
    </SelectButtonContext.Provider>
  );
};

const defaultSx: Sx = {
  flexDirection: 'row',
};

const defaultSelectedSx: Sx = {
  bg: 'secondary',
};

const defaultButtonSx: Sx = {
  margin: 2,
};

SelectButton.OptionButton = OptionButton;

export type { OptionButtonProps };
