/**
 * File: /src/components/SelectButton/SelectButton.stories.tsx
 * Project: @native-theme-ui/extra
 * File Created: 13-12-2022 10:39:17
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 13:06:01
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { SelectButton } from './index';
import { autoContrastArgType, createArgsStory, createSxArgs, sxArgTypes } from '../../../storybook';

export default {
  title: 'components/forms/SelectButton',
  component: SelectButton,
  parameters: {
    status: { type: 'beta' },
  },
};

export const select = createArgsStory(SelectButton, { onValueChange: action('onValueChange') }, [
  <SelectButton.OptionButton>BMW</SelectButton.OptionButton>,
  <SelectButton.OptionButton>AUDI</SelectButton.OptionButton>,
  <SelectButton.OptionButton>FORD</SelectButton.OptionButton>,
  <SelectButton.OptionButton>SUZUKI</SelectButton.OptionButton>,
]);
select.args = {
  autoContrast: SelectButton.defaultProps?.autoContrast,
  ...createSxArgs(),
};
select.argTypes = {
  selectedValue: { control: { type: 'text' } },
  autoContrast: {
    options: ['A', 'AA', 'AAA', 'false'],
    control: { type: 'select' },
    ...autoContrastArgType,
    ...sxArgTypes,
  },
};
