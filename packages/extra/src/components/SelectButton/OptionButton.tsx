/**
 * File: /src/components/SelectButton/OptionButton.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-12-2022 10:55:04
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 13:01:42
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, useContext, useState, useEffect, useCallback } from 'react';
import { Button, ButtonProps } from '@native-theme-ui/core';
import { useCalculatedSx } from '@native-theme-ui/dripsy';
import { SelectButtonContext } from './context';

export type OptionButtonProps = Omit<ButtonProps, 'children'> & {
  value?: number | string;
  children: string | number;
};

export const OptionButton: FC<OptionButtonProps> = (props: OptionButtonProps) => {
  const [index, setIndex] = useState<number>();
  const [selected, setSelected] = useState(false);
  const { selectedIndex, setValues, selectedSx, buttonSx, setSelectedIndex } = useContext(SelectButtonContext);
  const sx = {
    ...useCalculatedSx(buttonSx),
    ...useCalculatedSx(selected ? selectedSx : {}),
    ...useCalculatedSx(props.sx),
  };

  useEffect(() => {
    if (typeof index === 'number' || !setValues) return;
    setValues((values: (string | number)[]) => {
      setIndex(values.length);
      return [...values, props.value ?? props.children];
    });
  }, [index, setValues]);

  useEffect(() => {
    setSelected(typeof selectedIndex !== 'undefined' && selectedIndex === index);
  }, [selectedIndex]);

  const handlePress = useCallback(() => {
    if (setSelectedIndex) setSelectedIndex(index);
    if (props.onPress) props.onPress();
  }, [setSelectedIndex, index, props.onPress]);

  return <Button {...props} sx={sx} onPress={handlePress} />;
};
