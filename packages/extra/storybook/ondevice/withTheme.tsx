/**
 * File: /storybook/ondevice/withTheme.tsx
 * Project: @native-theme-ui/core
 * File Created: 02-07-2022 13:20:19
 * Author: Clay Risser
 * -----
 * Last Modified: 02-11-2022 05:43:09
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { ReactNode } from 'react';
import { DripsyProvider } from 'dripsy';
import { makeDecorator, LegacyStoryFn, StoryWrapper } from '@storybook/addons';
import * as themes from '../../theme';

const logger = console;
const options = {
  themeName: 'main',
  autoContrast: 'A',
};
const theme = {
  ...(themes as any)[options.themeName],
  autoContrast: options.autoContrast,
};
logger.info('theme', theme);

const withTheme = makeDecorator({
  name: 'withTheme',
  parameterName: 'theme',
  skipIfNoParametersOrOptions: false,
  wrapper: ((
    getStory: LegacyStoryFn<ReactNode>,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    context: any,
  ) => {
    return <DripsyProvider theme={theme}>{getStory(context)}</DripsyProvider>;
  }) as StoryWrapper,
});

export default withTheme;
