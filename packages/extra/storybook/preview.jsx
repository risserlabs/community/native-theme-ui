/**
 * File: /storybook/preview.jsx
 * Project: @native-theme-ui/core
 * File Created: 23-01-2022 02:18:40
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:43:30
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { createContext, useContext } from 'react';
import { DripsyProvider } from 'dripsy';
import { View } from 'react-native';
import { themes as storybookThemes } from '@storybook/theming';
import { useFonts } from 'expo-font';
import { withDesign } from 'storybook-addon-designs';
import { withGlobals } from '@luigiminardim/storybook-addon-globals-controls';
import { withThemes } from 'storybook-addon-themes/react';
import * as themes from '../theme';
import fonts from './fonts';

const logger = console;
const GlobalValuesContext = createContext();

export const parameters = {
  globalsControls: {},
  controls: { expanded: true },
  status: {
    statuses: {},
  },
  darkMode: {
    dark: { ...storybookThemes.dark },
    light: { ...storybookThemes.normal },
  },
  paddings: {
    default: 'Small',
  },
  backgrounds: [
    { name: 'light', value: 'white', default: true },
    { name: 'dark', value: '#262626' },
  ],
  themes: {
    default: 'main',
    clearable: false,
    onChange: (theme) => {
      console.info('theme', theme);
    },
    list: Object.entries(themes).map(([name, theme]) => ({
      name,
      themeUI: theme,
      color: theme.colors.primary,
    })),
    Decorator: (props) => {
      const globalValuesContext = useContext(GlobalValuesContext);
      const theme = {
        ...(props?.theme?.themeUI || {}),
        ...(globalValuesContext.autoContrast
          ? {
              autoContrast:
                globalValuesContext.autoContrast.toLowerCase() === 'false' ? false : globalValuesContext.autoContrast,
            }
          : {}),
      };
      logger.info('theme', theme);
      useFonts(fonts);
      return <DripsyProvider theme={theme}>{props.children}</DripsyProvider>;
    },
  },
};

const withDisplayGlobals = withGlobals((Story, globalValues) => (
  <GlobalValuesContext.Provider value={globalValues}>
    <View style={{ flex: 1 }}>
      <Story />
    </View>
  </GlobalValuesContext.Provider>
));

export const decorators = [withThemes, withDesign, withDisplayGlobals];

export const globalTypes = {
  autoContrast: {
    options: ['A', 'AA', 'AAA', 'false'],
    defaultValue: 'A',
    control: { type: 'select' },
    toolbar: {
      icon: 'circle',
      items: ['A', 'AA', 'AAA', 'false'],
      showName: false,
      dynamicTitle: true,
    },
  },
};
