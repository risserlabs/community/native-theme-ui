/**
 * File: /src/index.ts
 * Project: @native-theme-ui/core
 * File Created: 13-12-2022 06:27:49
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:31:41
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { StyleProp, StyleSheet } from 'react-native';
import { styled as dripsyStyled } from 'dripsy';
import {
  ComponentType,
  ElementRef,
  ForwardRefExoticComponent,
  PropsWithChildren,
  PropsWithoutRef,
  RefAttributes,
} from 'react';
import { LProps, LSx } from './types';

export function styled<Props, C extends ComponentType<any> = ComponentType<Props>>(
  Component: C,
  { themeKey, defaultVariant }: { themeKey?: string; defaultVariant?: string } = {},
): <Extra>(
  defaultStyle?: LSx | ((props: Extra) => LSx) | undefined,
) => ForwardRefExoticComponent<PropsWithoutRef<PropsWithChildren<LProps<C, Extra>>> & RefAttributes<ElementRef<C>>> {
  // @ts-ignore
  return dripsyStyled(Component as any, { themeKey, defaultVariant } as any) as <Extra>(
    defaultStyle?: LSx | ((props: Extra) => LSx) | undefined,
  ) => ForwardRefExoticComponent<PropsWithoutRef<PropsWithChildren<LProps<C, Extra>>> & RefAttributes<ElementRef<C>>>;
}

export function lookupStyle<T>(style: StyleProp<unknown> | undefined, key: string): T | undefined {
  if (typeof style === 'undefined') return;
  const flattenedStyle = StyleSheet.flatten(style);
  if (typeof flattenedStyle !== 'object') return;
  return (flattenedStyle as Record<string, unknown>)[key] as T;
}

export function forwardThemeKey(props: { themeKey?: any } & any, themeKey: string): undefined {
  return props.themeKey || themeKey;
}

export * from './hooks';
export * from './types';
