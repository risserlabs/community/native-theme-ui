/**
 * File: /src/types.ts
 * Project: @native-theme-ui/dripsy
 * File Created: 27-12-2022 09:11:44
 * Author: Clay Risser
 * -----
 * Last Modified: 04-01-2023 02:14:47
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { ThemeUICSSProperties } from '@theme-ui/css';
import type { Component, ComponentType, PropsWithoutRef, PropsWithRef, ReactNode, RefAttributes } from 'react';
import type { ViewStyle, TextStyle } from 'react-native';
import type { TextShadows } from '@dripsy/core/build/declarations';
import type { Shadows } from '@dripsy/core/build/declarations';
import type { DripsyFinalTheme } from '@dripsy/core/build/declarations';

type HiddenArrayKeys = Exclude<keyof [], number>;
type SafeTokenized<
  Theme,
  IsFirstIteration extends boolean,
  Key extends Extract<keyof Theme, string | number>,
  AllowsRootKeys extends boolean,
> = IsFirstIteration extends true
  ? AllowsRootKeys extends false
    ? Tokenize<Theme[Key], false, false>
    : Tokenize<Theme[Key], false, false> | `${Key}.${Tokenize<Theme[Key], false, false>}`
  : `${Key}.${Tokenize<Theme[Key], IsFirstIteration>}`;
type Tokenize<Theme, IsFirstIteration extends boolean, AllowsRootKeys extends boolean = true> = Extract<
  keyof {
    [Key in Exclude<Extract<keyof Theme, string | number>, HiddenArrayKeys> as Theme[Key] extends
      | string
      | number
      | ''
      | never
      | undefined
      | null
      ? Key extends number
        ? Key
        : `${Key}`
      : `${Key}` | SafeTokenized<Theme, IsFirstIteration, Key, AllowsRootKeys>]: true;
  },
  string | number | '' | never | undefined | null
>;
export type ValueOrThemeFactoryValue<T> = T | ((theme: DripsyFinalTheme) => T);
export type ResponsiveValue<T> = T | (null | T)[] | null | undefined;
type CssPropertyNames = {
  [key in keyof ThemeUICSSProperties]: key;
};
type SpecialCssProperty<Key extends keyof CssPropertyNames> = NonNullable<CssPropertyNames[Key]>;
type BoxShadow = SpecialCssProperty<'boxShadow'>;
type TextShadow = SpecialCssProperty<'textShadow'>;
export type WebShadowStyles = {
  [key in BoxShadow]?: string;
} & {
  [key in TextShadow]?: string;
};
export type ShadowStyleKeys = keyof WebShadowStyles;
export type StyleableSxProperties =
  | Exclude<keyof ThemeUICSSProperties, ShadowStyleKeys | 'variant'>
  | keyof ViewStyle
  | keyof TextStyle;
type SmartOmit<T, K extends keyof T> = Omit<T, K>;
type ReactNativeShadowStyles = SmartOmit<Shadows, 'shadowColor'> & {
  shadowColor?: string & {};
};
type ReactNativeTextShadowStyles = SmartOmit<TextShadows, 'textShadowColor'> & {
  textShadowColor?: string & {};
};
type ReactNativeOnlyStyles = Partial<
  ReactNativeShadowStyles &
    ReactNativeTextShadowStyles & {
      transform?: ViewStyle['transform'] | ThemeUICSSProperties['transform'];
    } & {
      animationKeyframes?: Record<string, unknown>;
    }
>;
type SxStyles = {
  [key in StyleableSxProperties]?: any;
};
interface SxVariantStyles {
  variant?: string;
}

export type LSx = WebShadowStyles & ReactNativeOnlyStyles & SxVariantStyles & SxStyles;
export type LSxProp = LSx | ((theme: DripsyFinalTheme) => LSx);
export interface LStyledProps {
  as?: ComponentType<any>;
  variant?: string;
  themeKey?: string;
  sx?: LSxProp;
  variants?: string[];
}
export type LDefaultStyleKey = 'defaultStyle';
export type LThemedOptions<ExtraProps> = {
  [key in LDefaultStyleKey]?: LSx | ((props: ExtraProps) => LSx);
} & {
  defaultVariant?: string & {};
  defaultVariants?: (string & {})[];
  themeKey?: string;
};

type MergeProps<P1, P2> = Omit<P1, keyof P2> & P2;
type PropsWithoutVariants<P> = Omit<P, 'variant' | 'variants'>;
type LPropsWithStyledProps<P> = P & LStyledProps;
export declare type LProps<C, ExtraProps> = C extends ComponentType<infer BaseProps>
  ? MergeProps<PropsWithoutVariants<BaseProps>, LPropsWithStyledProps<ExtraProps>>
  : never;

export type LDripsyStyledProps<Props> = LStyledProps &
  (
    | (PropsWithoutRef<Props> & RefAttributes<Component<Props, any, any>>)
    | PropsWithRef<
        Props & {
          children?: ReactNode;
        }
      >
  );

export type Sx = LSx;
export type SxProp = LSxProp;
export type DripsyStyledProps<P> = LDripsyStyledProps<P>;
