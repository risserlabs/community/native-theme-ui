/**
 * File: /transpileModules.js
 * Project: @native-theme-ui/core
 * File Created: 19-10-2022 06:01:04
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 10:10:06
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

module.exports = [
  ...require('@native-theme-ui/auto-contrast/transpileModules'),
  ...require('@native-theme-ui/dripsy/transpileModules'),
  '@motify/components',
  '@motify/core',
  '@motify/interactions',
  '@native-theme-ui/core',
  '@native-theme-ui/auto-contrast',
  'framer-motion',
  'moti',
  'react-native-gesture-handler',
  'react-native-reanimated',
  'react-native-swipe-gestures',
];
