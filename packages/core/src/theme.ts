/**
 * File: /src/theme.ts
 * Project: @native-theme-ui/core
 * File Created: 17-08-2022 11:19:50
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:53:12
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DripsyFinalTheme } from 'dripsy';

export const variants: Partial<DripsyFinalTheme> = {
  text: {
    h1: {},
    h2: {},
    h3: {},
    h4: {},
    h5: {},
    h6: {},
    heading: {},
    paragraph: {},
  },
  variants: {
    primary: {},
  },
  layout: {
    container: {},
  },
  images: {
    avatar: {},
  },
  badges: {
    accent: {},
    circle: {
      borderRadius: 9999,
    },
    outline: {},
  },
  alerts: {
    primary: {
      color: 'background',
      bg: 'primary',
    },
    muted: {
      color: 'text',
      bg: 'muted',
    },
  },
  buttons: {
    primary: {},
    icon: {},
  },
};

export interface ExtendedCustomTheme {
  autoContrast?: import('@native-theme-ui/auto-contrast').AutoContrast;
  text: {
    h1: Record<string, unknown>;
    h2: Record<string, unknown>;
    h3: Record<string, unknown>;
    h4: Record<string, unknown>;
    h5: Record<string, unknown>;
    h6: Record<string, unknown>;
    heading: Record<string, unknown>;
    paragraph: Record<string, unknown>;
  };
  variants: {
    primary: Record<string, unknown>;
  };
  table: {
    primary: Record<string, unknown>;
  };
  layout: {
    container: Record<string, unknown>;
  };
  images: {
    avatar: Record<string, unknown>;
  };
  badges: {
    accent: Record<string, unknown>;
    circle: Record<string, unknown>;
    outline: Record<string, unknown>;
  };
  alerts: {
    primary: Record<string, unknown>;
    muted: Record<string, unknown>;
  };
  buttons: {
    primary: Record<string, unknown>;
    icon: Record<string, unknown>;
  };
}

declare module 'dripsy' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DripsyCustomTheme extends ExtendedCustomTheme {}
}
