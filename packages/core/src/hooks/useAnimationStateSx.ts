/**
 * File: /src/hooks/useAnimationStateSx.ts
 * Project: @native-theme-ui/core
 * File Created: 02-09-2022 06:26:21
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:49:37
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { SxProp, useSx } from '@native-theme-ui/dripsy';
import { useAnimationState, UseAnimationStateConfig } from 'moti';

export type VariantsSx = Record<string, SxProp>;

export function useAnimationStateSx(variantsSx: VariantsSx, config?: UseAnimationStateConfig<any>, themeKey?: any) {
  const sx = useSx();
  const variants = Object.entries(variantsSx).reduce((variants: any, [key, value]: [string, SxProp]) => {
    variants[key] = sx(value, { themeKey });
    return variants;
  }, {});
  return useAnimationState(variants, config);
}
