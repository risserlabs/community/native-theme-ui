/**
 * File: /src/util.ts
 * Project: @native-theme-ui/core
 * File Created: 28-02-2022 07:21:50
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:30:09
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Platform, StyleProp, StyleSheet } from 'react-native';
import { Sx, SxProp, useCalculatedSx } from '@native-theme-ui/dripsy';
import { MotiPressableProps } from 'moti/interactions';

export function createUseSplitProps<
  Props,
  SplitPropsBuckets = Record<string, Record<string, unknown>>,
  SplitSxBuckets = Record<string, Partial<SxProp>>,
  SplitStyleBuckets = Record<string, Partial<SxProp>>,
>(propsBuckets?: Record<string, Matcher> | null, sxBuckets?: Record<string, Matcher> | null) {
  const lastPropsBucketId = 'baseProps';
  const lastSxBucketId = 'baseSx';
  const lastStyleBucketId = 'baseStyle';
  return (props: Props, sx?: Sx): SplitPropsBuckets & SplitSxBuckets & SplitStyleBuckets => {
    if (!sx) sx = useCalculatedSx((props as { sx?: SxProp }).sx || {});
    const style = StyleSheet.flatten((props as { style?: StyleProp<Record<string, unknown>> }).style || {});
    const propsMap: Record<string, Record<string, unknown>> = {};
    const styleMap: Record<string, Record<string, unknown>> = {};
    const sxMap: Record<string, Record<string, unknown>> = {};
    if (lastPropsBucketId) propsMap[lastPropsBucketId] = {};
    if (lastSxBucketId) sxMap[lastSxBucketId] = {};
    if (lastStyleBucketId) styleMap[lastStyleBucketId] = {};
    if (propsBuckets) {
      Object.keys(propsBuckets).forEach((propsBucketId: string) => {
        propsMap[propsBucketId] = {};
      });
      const clonedProps = { ...props };
      const propsBucketsIds = Object.keys(propsBuckets);
      Object.entries(clonedProps as any).forEach(([key, prop]: [string, unknown]) => {
        for (const propsBucketId of propsBucketsIds) {
          const propsSet = propsMap[propsBucketId];
          const matcher = propsBuckets[propsBucketId];
          if (match(matcher, key, prop)) {
            propsSet[key] = prop;
            return;
          }
        }
        if (lastPropsBucketId && key !== 'sx') propsMap[lastPropsBucketId][key] = prop;
      });
    }
    if (sxBuckets) {
      Object.keys(sxBuckets).forEach((sxBucketId: string) => {
        sxMap[sxBucketId] = {};
        styleMap[`${sxBucketId.substring(0, sxBucketId.length - 2)}Style`] = {};
      });
      const sxBucketsIds = Object.keys(sxBuckets);
      Object.entries(sx).forEach(([key, value]: [string, unknown]) => {
        for (const sxBucketId of sxBucketsIds) {
          const sxSet = sxMap[sxBucketId];
          const matcher = sxBuckets[sxBucketId];
          if (match(matcher, key, value)) {
            sxSet[key] = value;
            return;
          }
        }
        if (lastSxBucketId) sxMap[lastSxBucketId][key] = value;
      });
      Object.entries(style).forEach(([key, value]: [string, unknown]) => {
        for (const sxBucketId of sxBucketsIds) {
          const styleSet = styleMap[`${sxBucketId.substring(0, sxBucketId.length - 2)}Style`];
          const matcher = sxBuckets[sxBucketId];
          if (match(matcher, key, value)) {
            styleSet[key] = value;
            return;
          }
        }
        if (lastStyleBucketId) styleMap[lastStyleBucketId][key] = value;
      });
    }
    return {
      ...(propsMap as unknown as SplitPropsBuckets),
      ...(sxMap as unknown as SplitSxBuckets),
      ...(styleMap as unknown as SplitStyleBuckets),
    };
  };
}

function match(matcher: Matcher, key: string, value: unknown): boolean {
  if (Array.isArray(matcher)) {
    for (const matcherItem of matcher) {
      if (match(matcherItem, key, value)) return true;
    }
  }
  if (matcher instanceof Set) return matcher.has(key);
  if (matcher instanceof RegExp) return !!key.match(matcher);
  return matcher === key;
}

export type Matcher = Set<string> | RegExp | string | (RegExp | string | Set<string>)[];

const marginArr = ['m', 'mb', 'ml', 'mr', 'mt', 'mx', 'my'];
const marginRegExp = /^(margin)/g;
const paddingArr = ['p', 'pb', 'pl', 'pr', 'pt', 'px', 'py'];
const paddingRegExp = /^(padding)/g;
export const splitPropsGroups: {
  sx: {
    inner: (string | RegExp | Set<string>)[];
    margin: (string | RegExp | Set<string>)[];
    outer: (string | RegExp | Set<string>)[];
    padding: (string | RegExp | Set<string>)[];
    text: (string | RegExp | Set<string>)[];
  };
  props: {
    moti: (string | RegExp | Set<string>)[];
    pressable: (string | RegExp | Set<string>)[];
  };
} = {
  sx: {
    margin: [new Set(marginArr), marginRegExp],
    padding: [new Set(paddingArr), paddingRegExp],
    text: [/^(font)|(text)|(color)/g],
    outer: [new Set(['alignSelf', 'justifySelf', 'placeSelf', ...marginArr]), marginRegExp],
    inner: [
      /^flex/g,
      new Set(['alignContent', 'alignItems', 'justifyContent', 'placeContent', 'placeItems', ...paddingArr]),
      paddingRegExp,
    ],
  },
  props: {
    moti: [
      new Set([
        'animate',
        'animateInitialState',
        'animateSx',
        'delay',
        'exit',
        'exitSx',
        'exitTransition',
        'from',
        'fromSx',
        'onDidAnimate',
        'state',
        'stylePriority',
        'transition',
      ]),
    ],
    pressable: ['onHoverIn', 'onHoverOut', 'onKeyDown', 'onKeyUp', 'onLongPress', 'onPress', 'onPressIn', 'onPressOut'],
  },
};

export function isText(children: unknown) {
  if (Array.isArray(children)) {
    return (
      children.length > 1 &&
      typeof children[0] === 'string' &&
      '_owner' in children[1] &&
      '_store' in children[1] &&
      'key' in children[1] &&
      'props' in children[1] &&
      'ref' in children[1] &&
      'type' in children[1]
    );
  }
  return typeof children === 'string';
}

export type Args = any;

export const passthroughStyle: Sx = {
  alignItems: 'center',
  display: 'flex',
  justifyContent: 'center',
  margin: 0,
  overflow: 'visible',
  alignSelf: 'flex-start',
  width: '100%',
  padding: 0,
  ...Platform.select({ web: { height: '100%' } }),
};

export const bottomStyle: Sx = {
  margin: 0,
  overflow: 'visible',
  width: '100%',
  ...Platform.select({ web: { height: '100%' } }),
};

export type PropsPressableGroup = Pick<
  MotiPressableProps,
  'onHoverIn' | 'onHoverOut' | 'onKeyDown' | 'onKeyUp' | 'onLongPress' | 'onPress' | 'onPressIn' | 'onPressOut'
>;
