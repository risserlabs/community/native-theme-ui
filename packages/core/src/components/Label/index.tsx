/**
 * File: /src/components/Label/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:40:50
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ComponentProps } from 'react';
import { Sx, styled } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Text } from '../Text';

const themeKey = 'forms';

const defaultStyle: Sx = {
  display: 'flex',
  fontWeight: '700',
  width: '100%',
};

const StyledText = styled(Text, {
  themeKey,
  defaultVariant: 'label',
})(defaultStyle);

export type LabelProps = ComponentProps<typeof StyledText>;

export const Label: FC<LabelProps> = (props: LabelProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  return <StyledText {...props} sx={sx} />;
};
