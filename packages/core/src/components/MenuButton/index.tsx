/**
 * File: /src/components/MenuButton/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 19-08-2022 08:29:43
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:35:35
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Svg, Path } from 'react-native-svg';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { IconButton, IconButtonProps } from '../IconButton';

export const menuIcon = ({ color = '#000000', size = 24 }) => {
  return (
    <Svg viewBox="0 0 24 24" width={size} height={size} fill="currentColor" color={color}>
      {/* eslint-disable-next-line spellcheck/spell-checker */}
      <Path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z" />
    </Svg>
  );
};

export type MenuButtonProps = Omit<IconButtonProps, 'children'>;

export const MenuButton: FC<MenuButtonProps> = (props: MenuButtonProps) => {
  const sx = useAutoContrast(props, {}, 'buttons');
  return (
    <IconButton {...props} sx={sx}>
      {menuIcon({ color: sx.color as string, size: props.size })}
    </IconButton>
  );
};
