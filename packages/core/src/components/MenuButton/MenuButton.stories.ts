/**
 * File: /src/components/MenuButton/MenuButton.stories.ts
 * Project: @native-theme-ui/core
 * File Created: 19-06-2022 06:50:53
 * Author: K S R P BHUSHAN
 * -----
 * Last Modified: 22-08-2022 07:39:53
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { action } from '@storybook/addon-actions';
import { createArgsStory, sxArgTypes, createSxArgs, autoContrastArgType } from '../../../storybook';
import { MenuButton } from './index';

export default {
  title: 'components/buttons/MenuButton',
  component: MenuButton,
  parameters: {},
};

export const menuButton = createArgsStory(MenuButton, {
  onPress: action('I was pressed'),
  onLongPress: action('I was long pressed'),
  onPressIn: action('I was pressed in'),
  onPressOut: action('I was pressed out'),
});
menuButton.args = {
  autoContrast: undefined,
  ...createSxArgs(),
};
menuButton.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};
