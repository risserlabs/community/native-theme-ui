/**
 * File: /src/components/Paragraph/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 20-06-2022 05:48:50
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:33:01
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Sx, styled } from '@native-theme-ui/dripsy';
import { em } from '@expo/html-elements/build/css/units';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Text, TextProps } from '../Text';

const themeKey = 'text';

const defaultStyle: Sx = {
  fontFamily: 'body',
  fontWeight: 'body',
  my: em(1),
};

const StyledText = styled(Text, {
  themeKey,
  defaultVariant: 'paragraph',
})(defaultStyle);

export type ParagraphProps = TextProps;

export const Paragraph: FC<ParagraphProps> = (props: ParagraphProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return <StyledText {...(props as any)} sx={sx} />;
};
