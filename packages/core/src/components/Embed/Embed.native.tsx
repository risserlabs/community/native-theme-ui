/**
 * File: /src/components/Embed/Embed.native.tsx
 * Project: @native-theme-ui/core
 * File Created: 18-07-2022 06:25:17
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:06:20
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import WebView from 'react-native-webview';
import { styled, useCalculatedSx } from '@native-theme-ui/dripsy';
import YoutubeIframeThemed from './YoutubeIframeThemed';
import { AspectRatio } from '../AspectRatio';
import { useSplitProps, EmbedProps } from './props';

const StyledWebView = styled(WebView)({});

export const Embed: FC<EmbedProps> = (props: EmbedProps) => {
  const sx = {
    border: props.frameBorder,
    ...useCalculatedSx(props.sx),
  };
  const { baseProps, iframeProps } = useSplitProps(props, sx);

  const youtubeVideoId =
    (typeof props.src === 'string' &&
      (props.src.indexOf('youtube.com/') > -1 || props.src.indexOf('youtube/') > -1) &&
      Array.from(props.src.match(/[^/]+$/g) || [])
        .join('')
        .trim()) ||
    null;

  function renderWebView() {
    if (!props.src) return [];
    if (youtubeVideoId) {
      const youtubeIframeThemedProps = { ...props };
      delete youtubeIframeThemedProps.src;
      return (
        <YoutubeIframeThemed
          initialPlayerParams={{
            ...(typeof iframeProps.allowFullScreen === 'boolean'
              ? { preventFullScreen: !iframeProps.allowFullScreen }
              : {}),
          }}
          videoId={youtubeVideoId}
          useLocalHTML
          style={{
            height: '100%',
            width: '100%',
          }}
        />
      );
    }
    return (
      <StyledWebView
        source={{ uri: props.src }}
        originWhitelist={['*']}
        sx={{
          width: sx.width,
          height: sx.height,
        }}
      />
    );
  }

  return (
    <AspectRatio {...baseProps} sx={sx}>
      {renderWebView()}
    </AspectRatio>
  );
};

Embed.defaultProps = {
  ratio: 16 / 9,
};
