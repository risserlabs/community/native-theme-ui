/**
 * File: /src/components/Embed/props.ts
 * Project: @native-theme-ui/core
 * File Created: 18-07-2022 06:36:25
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:52:12
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Sx } from '@native-theme-ui/dripsy';
import { AspectRatioProps } from '../AspectRatio';
import { createUseSplitProps } from '../../util';

export type BasePropsBucket = AspectRatioProps;

export interface IframePropsBucket {
  allow?: string;
  allowFullScreen?: boolean;
  frameBorder?: number;
  height?: string | number;
  src?: string;
  width?: string | number;
}

export const useSplitProps = createUseSplitProps<
  EmbedProps,
  {
    baseProps: BasePropsBucket;
    iframeProps: IframePropsBucket;
  },
  {
    baseSx: Sx;
  },
  {
    baseStyle: Record<string, unknown>;
  }
>({
  iframeProps: ['allow', 'allowFullScreen', 'frameBorder', 'height', 'src', 'width'],
});

export type EmbedProps = Omit<BasePropsBucket & IframePropsBucket, 'autoContrast' | 'children'>;
