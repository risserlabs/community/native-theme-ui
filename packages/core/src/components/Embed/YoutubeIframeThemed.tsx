/**
 * File: /src/components/Embed/YoutubeIframeThemed.tsx
 * Project: @native-theme-ui/core
 * File Created: 18-07-2022 08:05:40
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:51:59
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import YoutubeIframe, { YoutubeIframeProps } from 'react-native-youtube-iframe';
import { StyleSheet } from 'react-native';
import { SxProp, styled } from '@native-theme-ui/dripsy';

type YoutubeIframeStyledProps = Omit<YoutubeIframeProps, 'width' | 'height' | 'webViewStyle'> & {
  style?: any;
};

const YoutubeIframeStyled: FC<YoutubeIframeStyledProps> = (props: YoutubeIframeStyledProps) => {
  const style = StyleSheet.flatten(props.style || {});
  const webViewStyle = { ...style };
  delete webViewStyle.width;
  delete webViewStyle.height;
  return <YoutubeIframe {...props} height={style?.height} width={style?.width} webViewStyle={webViewStyle} />;
};

export type YoutubeIframeThemedProps = YoutubeIframeStyledProps & {
  sx?: SxProp;
};

const YoutubeIframeThemed = styled(YoutubeIframeStyled)();

export default YoutubeIframeThemed;
