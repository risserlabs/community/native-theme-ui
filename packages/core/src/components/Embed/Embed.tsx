/**
 * File: /src/components/Embed/Embed.tsx
 * Project: @native-theme-ui/core
 * File Created: 18-07-2022 06:25:17
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:07:51
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { useCalculatedSx } from '@native-theme-ui/dripsy';
import { AspectRatio } from '../AspectRatio';
import { useSplitProps, EmbedProps } from './props';

export const Embed: FC<EmbedProps> = (props: EmbedProps) => {
  const sx = {
    border: props.frameBorder,
    ...useCalculatedSx(props.sx),
  };
  const { baseProps, iframeProps } = useSplitProps(props, sx);

  return (
    <AspectRatio {...baseProps} sx={sx}>
      <iframe
        {...iframeProps}
        style={{
          position: 'absolute',
          width: '100%',
          height: '100%',
          top: 0,
          bottom: 0,
          left: 0,
          border: 0,
        }}
      />
    </AspectRatio>
  );
};

Embed.defaultProps = {
  ratio: 16 / 9,
};
