/**
 * File: /src/components/IconButton/IconButton.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 29-08-2022 14:19:18
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Svg, Circle } from 'react-native-svg';
import { action } from '@storybook/addon-actions';
import { createArgsStory, sxArgTypes, createSxArgs, autoContrastArgType } from '../../../storybook';
import { IconButton } from './index';
import { Image } from '../Image';

export default {
  title: 'components/buttons/IconButton',
  component: IconButton,
  parameters: {
    status: { type: 'beta' },
  },
};

export const iconButton = createArgsStory(IconButton, {
  onPress: action('I was pressed'),
  onLongPress: action('I was long pressed'),
  onPressIn: action('I was pressed in'),
  onPressOut: action('I was pressed out'),
});
iconButton.args = {
  children: (
    <Svg viewBox="0 0 24 24" width="24" height="24" fill="black">
      <Circle r={11} cx={12} cy={12} fill="none" stroke="black" strokeWidth={2} />
    </Svg>
  ),
  autoContrast: undefined,
  ...createSxArgs(),
};
iconButton.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const notificationIconButton = () => (
  <IconButton onPress={action('on press')}>
    <Image src={require('../../../assets/notificationIcon.png')} sx={{ width: 24, height: 24 }} />
  </IconButton>
);

export const likeThemeUi = () => (
  <IconButton onPress={action('on press')}>
    <Svg viewBox="0 0 24 24" width="24" height="24" fill="black">
      <Circle r={11} cx={12} cy={12} fill="none" stroke="black" strokeWidth={2} />
    </Svg>
  </IconButton>
);
