/**
 * File: /src/components/IconButton/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:34:41
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Button, ButtonProps } from '../Button';
import { useCalculatedSx, styled } from '@native-theme-ui/dripsy';

const StyledButton = styled(Button, {
  themeKey: 'buttons',
  defaultVariant: 'icon',
})({
  alignItems: 'center',
  appearance: 'none',
  backgroundColor: 'transparent',
  border: 'none',
  borderRadius: 4,
  justifyContent: 'center',
  pb: 1,
  pl: 1,
  pr: 1,
  pt: 1,
});

export type IconButtonProps = ButtonProps & {
  size?: number;
};

export const IconButton: FC<IconButtonProps> = (props: IconButtonProps) => {
  const sx = useCalculatedSx({
    width: props.size,
    height: props.size,
    ...(props.sx || {}),
  });
  return <StyledButton {...props} sx={sx} />;
};

IconButton.defaultProps = {
  size: 32,
};
