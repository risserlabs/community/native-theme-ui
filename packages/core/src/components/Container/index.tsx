/**
 * File: /src/components/Container/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 17-06-2022 13:07:00
 * Author: ajithkrm6
 * -----
 * Last Modified: 27-12-2022 10:59:00
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Sx, styled, forwardThemeKey } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Box, BoxProps } from '../Box';

const themeKey = 'layout';

const defaultStyle: Sx = {
  // maxWidth: "container",
  // mx: "auto",
  width: '100%',
};

const StyledBox = styled(Box, { themeKey, defaultVariant: 'container' })(defaultStyle);

export type ContainerProps = BoxProps;

export const Container: FC<ContainerProps> = (props: ContainerProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  return (
    <StyledBox
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      {...(props as any)}
      sx={sx}
      themeKey={forwardThemeKey(props, themeKey)}
    />
  );
};
