/**
 * File: /src/components/Progress/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:40:36
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ComponentProps } from 'react';
import { styled, Sx } from '@native-theme-ui/dripsy';
import { Box } from '../Box';
import { useSplitProps, ProgressProps } from './props';

const themeKey = 'variants';

const defaultVariant = 'styles.progress';

const defaultParentBoxStyle: Sx = {
  appearance: 'none',
  bg: 'gray',
  border: 'none',
  borderRadius: 9999,
  height: 4,
  margin: 0,
  overflow: 'hidden',
  padding: 0,
  width: '100%',
};

const StyledBox = styled(Box, {
  themeKey,
  defaultVariant,
})(defaultParentBoxStyle);

export type StyledBoxProps = ComponentProps<typeof StyledBox>;

export const Progress: FC<ProgressProps> = (props: ProgressProps) => {
  const { baseProps, baseStyle, baseSx, colorStyle, colorSx } = useSplitProps(props);
  const max = props.max || 100;
  const value = props.value || 0;

  return (
    <StyledBox {...baseProps} sx={baseSx} style={baseStyle}>
      <Box
        sx={{
          backgroundColor: colorSx.color || 'primary',
          bg: colorSx.color || 'primary',
        }}
        style={{
          backgroundColor: colorStyle.color as string,
          height: '100%',
          width: `${Math.max(Math.min(value / max, 1), 0) * 100}%`,
        }}
      />
    </StyledBox>
  );
};

export type { ProgressProps };
