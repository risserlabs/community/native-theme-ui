/**
 * File: /src/components/Box/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 28-12-2022 06:30:19
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { BackgroundColorProvider } from '@native-theme-ui/auto-contrast';
import { MotiPressable } from 'moti/interactions';
import { MotiView } from 'moti';
import { Sx, styled, useSx } from '@native-theme-ui/dripsy';
import { View as RNView } from 'react-native';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { BoxProps, useSplitProps } from './props';
import { Text } from '../Text';
import { bottomStyle, isText, passthroughStyle } from '../../util';
import { useCalculateMoti } from '../../hooks';

const themeKey = 'variants';

const defaultOuterStyle: Sx = {
  boxSizing: 'border-box',
  flexShrink: 1,
};

const defaultInnerStyle: Sx = {
  boxSizing: 'border-box',
  display: 'flex',
  flexDirection: 'column',
  flexShrink: 1,
};

const StyledView = styled(RNView, {
  themeKey,
})(defaultInnerStyle);

const StyledMotiPressable = styled(MotiPressable, {
  themeKey,
})(defaultInnerStyle);

const StyledMotiView = styled(MotiView, {
  themeKey,
})(defaultInnerStyle);

export const Box: FC<BoxProps> = (props: BoxProps) => {
  const sx = useAutoContrast(props, {}, themeKey);
  const calcSx = useSx();
  const {
    baseProps,
    baseStyle,
    baseSx,
    innerStyle,
    innerSx,
    motiPressableOuterStyle,
    motiPressableOuterSx,
    motiProps,
    pressableProps,
    sharedStyle,
    sharedSx,
    textProps,
    textStyle,
    textSx,
  } = useSplitProps(props, sx);
  const isPressable = !!Object.keys(pressableProps).length;
  const isMoti = !!Object.keys(motiProps).length;
  const { animate, from, exit } = useCalculateMoti(props, themeKey, isMoti);

  const children = isText(props.children) ? (
    <Text {...textProps} sx={textSx} style={textStyle}>
      {props.children}
    </Text>
  ) : (
    props.children
  );

  const renderChildren = () => (
    <BackgroundColorProvider {...props} themeKey={themeKey}>
      {children}
    </BackgroundColorProvider>
  );

  if (isPressable) {
    if (isMoti) {
      const outerStyle = {
        ...defaultOuterStyle,
        ...calcSx(motiPressableOuterSx || {}, { themeKey }),
        ...calcSx(sharedSx || {}, { themeKey }),
        ...motiPressableOuterStyle,
        ...sharedStyle,
      };
      return (
        <MotiPressable
          {...pressableProps}
          containerStyle={outerStyle}
          style={{
            ...passthroughStyle,
            ...sharedStyle,
          }}
        >
          <StyledMotiView
            {...(baseProps as any)}
            {...motiProps}
            animate={animate}
            exit={exit}
            from={from}
            style={{
              ...bottomStyle,
              ...baseStyle,
              ...sharedStyle,
              ...innerStyle,
            }}
            sx={{ ...baseSx, ...sharedSx, ...innerSx }}
          >
            {renderChildren()}
          </StyledMotiView>
        </MotiPressable>
      );
    }
    const outerStyle = {
      ...defaultOuterStyle,
      ...calcSx(baseSx || {}, { themeKey }),
      ...calcSx(motiPressableOuterSx || {}, { themeKey }),
      ...calcSx(sharedSx || {}, { themeKey }),
      ...motiPressableOuterStyle,
      ...baseStyle,
      ...sharedStyle,
    };
    return (
      <StyledMotiPressable
        {...(baseProps as any)}
        {...pressableProps}
        containerStyle={outerStyle}
        style={{ ...bottomStyle, ...sharedStyle, ...innerStyle }}
        sx={{ ...sharedSx, ...innerSx }}
      >
        {renderChildren()}
      </StyledMotiPressable>
    );
  }
  if (isMoti) {
    return (
      <StyledMotiView
        {...(baseProps as any)}
        {...motiProps}
        animate={animate}
        exit={exit}
        from={from}
        style={{
          ...baseStyle,
          ...innerStyle,
          ...motiPressableOuterStyle,
          ...sharedStyle,
        }}
        sx={{
          ...baseSx,
          ...innerSx,
          ...motiPressableOuterSx,
          ...sharedSx,
        }}
      >
        {renderChildren()}
      </StyledMotiView>
    );
  }
  return (
    <StyledView
      {...baseProps}
      style={{
        ...baseStyle,
        ...innerStyle,
        ...motiPressableOuterStyle,
        ...sharedStyle,
      }}
      sx={{
        ...baseSx,
        ...innerSx,
        ...motiPressableOuterSx,
        ...sharedSx,
      }}
    >
      {renderChildren()}
    </StyledView>
  );
};

export type { BoxProps };
