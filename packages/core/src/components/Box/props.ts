/**
 * File: /src/components/Box/props.ts
 * Project: @native-theme-ui/core
 * File Created: 05-07-2022 06:24:30
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:30:18
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MotiPressableProps } from 'moti/interactions';
import { MotiProps } from 'moti';
import { Sx, SxProp, DripsyStyledProps } from '@native-theme-ui/dripsy';
import { TextProps } from '../Text';
import { createUseSplitProps, PropsPressableGroup, splitPropsGroups } from '../../util';

export type BasePropsBucket = Omit<DripsyStyledProps<MotiPressableProps>, 'state'> & MotiProps;

export type TextPropsBucket = TextProps;

export interface MotiPropsBucket {
  animateSx?: SxProp;
  exitSx?: SxProp;
  fromSx?: SxProp;
}

export const useSplitProps = createUseSplitProps<
  BoxProps,
  {
    baseProps: BasePropsBucket;
    motiProps: MotiPropsBucket;
    pressableProps: PropsPressableGroup;
    textProps: TextPropsBucket;
  },
  {
    baseSx: Sx;
    innerSx: Sx;
    motiPressableOuterSx: Sx;
    sharedSx: Sx;
    textSx: Sx;
  },
  {
    baseStyle: Record<string, unknown>;
    innerStyle: Record<string, unknown>;
    motiPressableOuterStyle: Record<string, unknown>;
    sharedStyle: Record<string, unknown>;
    textStyle: Record<string, unknown>;
  }
>(
  {
    motiProps: splitPropsGroups.props.moti,
    pressableProps: splitPropsGroups.props.pressable,
    textProps: 'autoContrast',
  },
  {
    innerSx: splitPropsGroups.sx.inner,
    motiPressableOuterSx: splitPropsGroups.sx.outer,
    sharedSx: ['borderRadius'],
    textSx: splitPropsGroups.sx.text,
  },
);

export type BoxProps = BasePropsBucket & TextPropsBucket & MotiPropsBucket;
