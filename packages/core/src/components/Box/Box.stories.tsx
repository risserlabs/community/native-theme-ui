/**
 * File: /src/components/Box/Box.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 24-11-2022 14:30:55
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Box } from './index';
import { Text } from '../Text';
import { useAnimationStateSx } from '../../hooks';
import { createArgsStory, createSxArgs, sxArgTypes, autoContrastArgType } from '../../../storybook';
import { action } from '@storybook/addon-actions';

export default {
  title: 'components/variants/Box',
  component: Box,
  parameters: {
    status: { type: 'beta' },
  },
};

export const box = createArgsStory(Box, {
  onHoverIn: action('onHoverOut'),
  onHoverOut: action('onHoverIn'),
  onLayout: action('onLayout'),
  onPress: action('onPress'),
  onPressIn: action('onPressIn'),
  onPressOut: action('onPressInOut'),
});
box.args = {
  children: 'I am a box',
  autoContrast: '',
  ...createSxArgs(),
};
box.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const likeThemeUi = () => (
  <Box
    sx={{
      p: 4,
      color: 'white',
      bg: 'primary',
    }}
  >
    <Text>Beep</Text>
  </Box>
);

export const multiple = () => {
  const style = {
    box: {
      color: 'white',
      height: 200,
      width: 200,
      margin: 4,
    },
    box1: {
      bg: 'primary',
    },
    box2: {
      bg: 'secondary',
    },
  };
  return (
    <Box sx={{ flexDirection: 'row' }}>
      <Box
        fromSx={{ bg: 'white' }}
        sx={style.box}
        animateSx={style.box1}
        transition={{ type: 'timing', duration: 1000 }}
      />
      <Box
        fromSx={{ bg: 'white' }}
        sx={style.box}
        animateSx={style.box2}
        transition={{ type: 'timing', duration: 2000 }}
      />
      <Box
        fromSx={{ bg: 'white' }}
        sx={style.box}
        animateSx={style.box1}
        transition={{ type: 'timing', duration: 3000 }}
      />
      <Box
        fromSx={{ bg: 'white' }}
        sx={style.box}
        animateSx={style.box2}
        transition={{ type: 'timing', duration: 4000 }}
      />
      <Box>
        <Box
          fromSx={{ bg: 'white' }}
          sx={style.box}
          animateSx={style.box1}
          transition={{ type: 'timing', duration: 5000 }}
        />
        <Box
          fromSx={{ bg: 'white' }}
          sx={style.box}
          animateSx={style.box2}
          transition={{ type: 'timing', duration: 6000 }}
        />
        <Box
          fromSx={{ bg: 'white' }}
          sx={style.box}
          animateSx={style.box1}
          transition={{ type: 'timing', duration: 7000 }}
        />
        <Box
          fromSx={{ bg: 'white' }}
          sx={style.box}
          animateSx={style.box2}
          transition={{ type: 'timing', duration: 8000 }}
        />
      </Box>
    </Box>
  );
};

export const animated = () => (
  <Box
    fromSx={{ bg: 'background' }}
    animateSx={{ bg: 'primary' }}
    sx={{
      alignItems: 'center',
      height: 460,
      justifyContent: 'center',
      padding: 4,
    }}
    transition={{ type: 'timing', duration: 5000 }}
  >
    <Box
      fromSx={{
        bg: 'muted',
        height: 0,
        width: 0,
        borderRadius: 9999,
      }}
      animateSx={{
        bg: 'secondary',
        width: 360,
        height: 360,
      }}
      sx={{
        borderRadius: 9999,
        elevation: 8,
        shadowColor: '#000',
        shadowOpacity: 1,
        shadowRadius: 8,
        shadowOffset: {
          height: 0,
          width: 0,
        },
      }}
      transition={{ type: 'timing', duration: 5000 }}
      onHoverIn={action('onHoverIn')}
      onHoverOut={action('onHoverOut')}
      onPress={action('onPress')}
      onPressIn={action('onPressIn')}
      onPressOut={action('onPressInOut')}
    />
  </Box>
);

export const heightFullPressable = () => {
  return (
    <Box
      sx={{
        height: 80,
        width: 80,
        backgroundColor: 'cyan',
      }}
      onPress={action('onPress')}
    >
      <Box sx={{ height: '100%', width: '100%', backgroundColor: 'yellow' }}>Hello</Box>
    </Box>
  );
};

export const inheritCursor = () => {
  return (
    <Box
      sx={{
        alignItems: 'center',
        bg: 'blue',
        cursor: 'pointer',
        height: 600,
        justifyContent: 'center',
        width: 600,
      }}
    >
      <Box sx={{ height: 300, width: 300, backgroundColor: 'yellow' }} />
    </Box>
  );
};

const Hover = () => {
  const animationState = useAnimationStateSx({
    from: {
      backgroundColor: 'muted',
      height: 0,
      width: 0,
      borderRadius: 9999,
    },
    to: {
      backgroundColor: 'secondary',
      width: 360,
      height: 360,
    },
  });
  return (
    <Box
      sx={{
        alignItems: 'center',
        bg: 'primary',
        height: 460,
        justifyContent: 'center',
        padding: 4,
      }}
    >
      <Box
        state={animationState}
        sx={{
          borderRadius: 9999,
          elevation: 8,
          shadowColor: '#000',
          shadowOpacity: 1,
          shadowRadius: 8,
          shadowOffset: {
            height: 0,
            width: 0,
          },
        }}
        transition={{ type: 'timing', duration: 1000 }}
        onHoverIn={() => {
          animationState.transitionTo('from');
          action('onHoverIn');
        }}
        onHoverOut={() => {
          animationState.transitionTo('to');
          action('onHoverOut');
        }}
        onPress={action('onPress')}
        onPressIn={() => {
          animationState.transitionTo('from');
          action('onPressIn');
        }}
        onPressOut={() => {
          animationState.transitionTo('to');
          action('onPressInOut');
        }}
      />
    </Box>
  );
};
export const hover = () => <Hover />;
