/**
 * File: /src/components/Field/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 27-06-2022 04:20:16
 * Author: Lavanya Katari
 * -----
 * Last Modified: 27-12-2022 12:53:59
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { styled } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Box, BoxProps } from '../Box';
import { Input } from '../Input';
import { Label } from '../Label';
import { useSplitProps, FieldProps } from './props';

const themeKey = 'forms';

const StyledParent = styled(Box, {
  themeKey,
  defaultVariant: 'field',
})({});

export type StyledParentProps = BoxProps;

export const Field: FC<FieldProps> = (props: FieldProps) => {
  const sx = useAutoContrast(props, {}, themeKey);
  const { baseProps, baseStyle, baseSx, textStyle, textSx, parentStyle, parentSx } = useSplitProps(props, sx);
  return (
    <StyledParent sx={parentSx} style={parentStyle}>
      <Label sx={textSx} style={textStyle}>
        {props.label}
      </Label>
      <Input {...baseProps} sx={{ ...baseSx, ...textSx } as any} style={{ ...baseStyle, ...textStyle }} />
    </StyledParent>
  );
};

export type { FieldProps };
