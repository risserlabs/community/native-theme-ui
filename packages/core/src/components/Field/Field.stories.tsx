/**
 * File: /src/components/Field/Field.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 27-06-2022 04:22:36
 * Author: Lavanya Katari
 * -----
 * Last Modified: 24-08-2022 09:44:36
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { createArgsStory, createSxArgs, sxArgTypes, autoContrastArgType } from '../../../storybook';
import { Field } from './index';
import { Checkbox } from '../Checkbox';

export default {
  title: 'Components/forms/Field',
  component: Field,
  parameters: {
    status: { type: 'beta' },
  },
};

export const field = createArgsStory(Field, {
  onBlur: action('onBlur'),
  onChange: action('onChange'),
  onFocus: action('onFocus'),
});
field.args = {
  label: 'Label',
  ...createSxArgs(),
  autoContrast: undefined,
};
field.argTypes = {
  ...sxArgTypes,
  ...autoContrastArgType,
};

export const likeThemeUi = () => <Field label="Email" />;

export const checkboxField = () => <Field as={Checkbox} label="Checkbox" />;
