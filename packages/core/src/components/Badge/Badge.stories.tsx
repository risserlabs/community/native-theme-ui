/**
 * File: /src/components/Badge/Badge.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 24-11-2022 12:18:37
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Avatar } from '../Avatar';
import { Badge } from './index';
import { Box } from '../Box';
import { createSxArgs, createArgsStory, sxArgTypes, autoContrastArgType } from '../../../storybook';

export default {
  title: 'components/badges/Badge',
  component: Badge,
  parameters: {
    status: { type: 'beta' },
  },
};

export const badge = createArgsStory(Badge);
badge.args = {
  children: 'Badge',
  autoContrast: undefined,
  ...createSxArgs(),
};
badge.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const likeThemeUi = () => <Badge>Badge</Badge>;

export const likeThemeUiCircle = () => (
  <Box sx={{ flexDirection: 'row' }}>
    <Avatar src="https://raw.githubusercontent.com/system-ui/theme-ui/stable/packages/docs/static/icon.png" />
    <Badge
      variant="circle"
      sx={{
        alignSelf: 'flex-end',
      }}
      style={{
        left: -20,
        bottom: -4,
      }}
    >
      16
    </Badge>
  </Box>
);
