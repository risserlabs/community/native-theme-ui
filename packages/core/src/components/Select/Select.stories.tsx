/**
 * File: /src/components/Select/Select.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 17-06-2022 06:16:36
 * Author: Harikittu46
 * -----
 * Last Modified: 13-12-2022 15:25:26
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { action } from '@storybook/addon-actions';
import React from 'react';
import { Args, autoContrastArgType, createArgsStory, createSxArgs, sxArgTypes } from '../../../storybook';
import { Box } from '../Box';
import { Select } from './index';

export default {
  title: 'components/forms/Select',
  component: Select,
  parameters: {
    status: { type: 'beta' },
  },
};

export const select = createArgsStory(Select, { onValueChange: action('onValueChange') }, [
  <Select.Option key="0" label="BMW" />,
  <Select.Option key="1" label="AUDI" />,
  <Select.Option key="2" label="FORD" />,
  <Select.Option key="3" label="SUZUKI" />,
]);
select.args = {
  autoContrast: Select.defaultProps?.autoContrast,
  ...createSxArgs(),
};
select.argTypes = {
  background: { control: { type: 'color' } },
  autoContrast: {
    options: ['A', 'AA', 'AAA', 'false'],
    control: { type: 'select' },
    ...autoContrastArgType,
    ...sxArgTypes,
  },
};

export const likeThemUi = (args: Args) => (
  <Box
    sx={{
      padding: 2,
      bg: args.background,
    }}
  >
    <Select>
      <Select.Option label="BMW" />
      <Select.Option label="AUDI" />
      <Select.Option label="FORD" />
      <Select.Option label="SUZUKI" />
    </Select>
  </Box>
);

export const withAutocontrast = (args: Args) => (
  <Select sx={{ bg: args.background }}>
    <Select.Option label="BMW" />
    <Select.Option label="AUDI" />
    <Select.Option label="FORD" />
    <Select.Option label="SUZUKI" />
  </Select>
);
withAutocontrast.args = {
  background: 'red',
  autoContrast: 'AAA',
};
withAutocontrast.argTypes = {
  ...autoContrastArgType,
  background: { control: { type: 'color' } },
  autoContrast: {
    options: ['A', 'AA', 'AAA', 'false'],
    control: { type: 'select' },
  },
};
