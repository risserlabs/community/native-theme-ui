/**
 * File: /src/components/Select/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 17-06-2022 06:47:48
 * Author: Harikittu46
 * -----
 * Last Modified: 27-12-2022 11:47:20
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { AutoContrast, useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Picker, PickerItemProps, PickerProps } from '@react-native-picker/picker';
import { Platform } from 'react-native';
import { SxProp, styled, Sx, DripsyStyledProps } from '@native-theme-ui/dripsy';

const themeKey = 'forms';

const defaultStyle: Sx = {
  color: 'text',
  // fontSize: "inherit",
  // lineHeight: "inherit",
  appearance: 'none',
  border: '1px solid',
  borderColor: 'blue',
  borderRadius: 4,
  display: 'flex',
  p: 2,
  width: '100%',
};

const StyledPicker = styled(Picker, {
  themeKey,
  defaultVariant: 'select',
})(({ showCursor }: { showCursor: boolean }) => ({
  ...Platform.select({
    web: {
      ...(showCursor ? { cursor: 'pointer' } : {}),
    },
  }),
  ...defaultStyle,
}));

type StyledPickerProps = DripsyStyledProps<PickerProps>;

export type SelectProps = Omit<StyledPickerProps, 'showCursor'> & {
  autoContrast?: AutoContrast;
  disabled?: boolean;
  sx?: SxProp;
};

export const Select: FC<SelectProps> & { Option: typeof Option } = (props: SelectProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey, true);
  return (
    <StyledPicker
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      {...(props as any)}
      showCursor={!!(props.accessibilityRole === 'link' || !props.disabled)}
      sx={sx}
    />
  );
};

const Option = Picker.Item;

Select.Option = Option;

export type SelectOptionProps = PickerItemProps;
