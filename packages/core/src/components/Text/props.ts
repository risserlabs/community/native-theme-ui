/**
 * File: /src/components/Text/props.ts
 * Project: @native-theme-ui/core
 * File Created: 05-07-2022 06:24:30
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:46:04
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { AutoContrast } from '@native-theme-ui/auto-contrast';
import { MotiProps } from 'moti';
import { TextProps as RNTextProps } from 'react-native';
import { DripsyStyledProps, SxProp } from '@native-theme-ui/dripsy';
import { createUseSplitProps, splitPropsGroups } from '../../util';

export type BasePropsBucket = DripsyStyledProps<RNTextProps> &
  MotiProps & {
    autoContrast?: AutoContrast;
  };

export interface MotiPropsBucket {
  animateSx?: SxProp;
  fromSx?: SxProp;
  exitSx?: SxProp;
}

export const useSplitProps = createUseSplitProps<
  TextProps,
  {
    baseProps: BasePropsBucket;
    motiProps: MotiPropsBucket;
  }
>({
  motiProps: splitPropsGroups.props.moti,
});

export type TextProps = BasePropsBucket & MotiPropsBucket;
