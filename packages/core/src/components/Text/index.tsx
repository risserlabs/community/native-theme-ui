/**
 * File: /src/components/Text/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:46:32
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { MotiText } from 'moti';
import { Text as RNText } from 'react-native';
import { styled } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { TextProps, useSplitProps } from './props';
import { useCalculateMoti } from '../../hooks';

const themeKey = 'text';

const defaultStyle = {
  fontFamily: 'body',
  fontWeight: 'body',
};

const StyledText = styled(RNText, {
  themeKey,
  defaultVariant: 'default',
})(defaultStyle);

const StyledMotiText = styled(MotiText, {
  themeKey,
  defaultVariant: 'default',
})(defaultStyle);

export const Text: FC<TextProps> = (props: TextProps) => {
  const sx = useAutoContrast(props, {}, themeKey);
  const { motiProps } = useSplitProps(props, sx);
  const isMoti = !!Object.keys(motiProps).length;
  const { animate, from, exit } = useCalculateMoti(props, themeKey, isMoti);

  if (!isMoti) return <StyledText {...(props as any)} sx={sx} />;
  return <StyledMotiText {...(props as any)} animate={animate} exit={exit} from={from} sx={sx} />;
};

export type { TextProps };
