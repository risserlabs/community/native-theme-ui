/**
 * File: /src/components/Textarea/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:02:31
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, useCallback, useState } from 'react';
import { StyleSheet, Platform } from 'react-native';
import { styled, Sx } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Input, InputProps } from '../Input';

const defaultStyle: Sx = {
  color: 'text',
  overflow: 'hidden',
};

const themeKey = 'forms';

const StyledInput = styled(Input, {
  themeKey,
  defaultVariant: 'textarea',
})(defaultStyle);

export type TextareaProps = InputProps & {
  rows?: number;
};

export const Textarea: FC<TextareaProps> = (props: TextareaProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  const [scrollHeight, setScrollHeight] = useState<string>();
  const style = StyleSheet.flatten(props.style) || {};

  const handleChange = useCallback(
    (e: any) => {
      if (Platform.OS === 'web') {
        setScrollHeight(
          `${(e.target.scrollHeight || 0) + (e.target.offsetHeight || 0) - (e.target.clientHeight || 0)}px`,
        );
      }
      if (props.onChange) props.onChange(e);
    },
    [props.onChange],
  );

  return (
    <StyledInput
      {...props}
      numberOfLines={props.numberOfLines ?? props.rows}
      onChange={handleChange}
      multiline
      sx={sx}
      style={{
        ...(Platform.select({
          web: {
            height: scrollHeight,
          },
        }) || {}),
        ...style,
      }}
    />
  );
};

Textarea.defaultProps = {
  rows: 4,
  textAlignVertical: 'top',
};
