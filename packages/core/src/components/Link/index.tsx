/**
 * File: /src/components/Link/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 05-08-2022 04:38:52
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:36:22
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ComponentProps } from 'react';
import { A } from '@expo/html-elements';
import { AutoContrast, useAutoContrast } from '@native-theme-ui/auto-contrast';
import { styled, Sx } from '@native-theme-ui/dripsy';

const themeKey = 'links';

const defaultStyle: Sx = {};

const StyledA = styled(A, {
  themeKey,
  defaultVariant: 'styles.a',
})(defaultStyle);

type StyledAProps = ComponentProps<typeof StyledA>;

export type LinkProps = Partial<
  StyledAProps & {
    autoContrast?: AutoContrast;
  }
>;

export const Link: FC<LinkProps> = (props: LinkProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  return <StyledA {...(props as StyledAProps)} sx={sx} />;
};
