/**
 * File: /src/components/Input/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:42:05
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, useCallback, useState } from 'react';
import { AutoContrast } from '@native-theme-ui/auto-contrast';
import { DripsyStyledProps, Sx, styled, forwardThemeKey, useThemeLookup } from '@native-theme-ui/dripsy';
import { TextInput, TextInputProps } from 'react-native';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';

const themeKey = 'forms';

const defaultStyle: Sx = {
  borderRadius: 4,
  borderStyle: 'solid',
  borderWidth: 1,
  color: 'text',
  fontSize: 1,
  p: 2,
};

const StyledTextInput = styled(TextInput, {
  themeKey,
  defaultVariant: 'input',
})(defaultStyle);

export type InputProps = Omit<DripsyStyledProps<TextInputProps>, 'children'> & {
  autoContrast?: AutoContrast;
  defaultValue?: string;
};

export const Input: FC<InputProps> = (props: InputProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  const themeLookup = useThemeLookup(forwardThemeKey(props, themeKey));
  const [value, setValue] = useState(props.defaultValue || '');

  const handleChangeText = useCallback(
    (text: string) => {
      setValue(text);
      if (props.onChangeText) props.onChangeText(text);
    },
    [props.onChangeText],
  );

  const clonedProps = { ...props };
  delete clonedProps.defaultValue;
  delete (clonedProps as any).children;

  return (
    <StyledTextInput
      {...(clonedProps as any)}
      onChangeText={handleChangeText}
      placeholderTextColor={themeLookup('color', props.placeholderTextColor)}
      selectionColor={themeLookup('color', props.selectionColor)}
      sx={sx}
      underlineColorAndroid={themeLookup('color', props.underlineColorAndroid)}
      value={props.value ?? value}
    />
  );
};
