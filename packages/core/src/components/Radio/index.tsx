/**
 * File: /src/components/Radio/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 20-06-2022 07:09:45
 * Author: Lavanya Katari
 * -----
 * Last Modified: 29-12-2022 11:58:02
 * Modified By: K S R P BHUSHAN
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, useState, useContext, useCallback, useEffect } from 'react';
import { Platform } from 'react-native';
import { Svg, Path, SvgProps } from 'react-native-svg';
import { styled, Sx } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Box, BoxProps } from '../Box';
import { RadioGroupContext, RadioIdContext } from './RadioGroup';
import { RadioProps, useSplitProps } from './props';

const themeKey = 'forms';

const defaultStyle: Sx = {};

const radioChecked = (props: SvgProps & { size: number }) => (
  <Svg width={props.size} height={props.size} viewBox="0 0 24 24" fill="currentColor" {...props}>
    {/* eslint-disable-next-line spellcheck/spell-checker */}
    <Path d="M12 7c-2.76 0-5 2.24-5 5s2.24 5 5 5 5-2.24 5-5-2.24-5-5-5zm0-5C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z" />
  </Svg>
);

const radioUnchecked = (props: SvgProps & { size: number }) => (
  <Svg width={props.size} height={props.size} viewBox="0 0 24 24" fill="currentColor" {...props}>
    {/* eslint-disable-next-line spellcheck/spell-checker */}
    <Path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z" />
  </Svg>
);

const radioIcon = (props: SvgProps & { size: number; checked: boolean }) => {
  if (props.checked) return radioChecked(props);
  return radioUnchecked(props);
};

const StyledBox = styled(Box, {
  themeKey,
  defaultVariant: 'radio',
})(({ showCursor }: { showCursor: boolean }) => ({
  ...Platform.select({
    web: {
      ...(showCursor ? { cursor: 'pointer' } : {}),
    },
  }),
  ...defaultStyle,
}));

export type StyledBoxProps = BoxProps;

export const Radio: FC<RadioProps> = (props: RadioProps) => {
  const [radioGroupIndex, setRadioGroupIndex] = useContext(RadioGroupContext);
  const radioId = useContext(RadioIdContext);
  const [checked, setChecked] = useState(props.defaultChecked ?? false);
  const sx = useAutoContrast(
    props,
    {
      ...defaultStyle,
      color: props.disabled ? 'gray' : 'primary',
    },
    themeKey,
  );
  const { baseProps, radioProps } = useSplitProps(props, sx);

  useEffect(() => {
    if (typeof radioGroupIndex === 'number' && radioGroupIndex !== radioId) {
      setChecked(false);
    }
  }, [radioGroupIndex]);

  const handlePress = useCallback(() => {
    if (props.disabled) return;
    setChecked((checked: boolean) => {
      if (!checked && typeof radioId === 'number') {
        setRadioGroupIndex(radioId);
      }
      return true;
    });
    if (props.onPress) props.onPress();
  }, []);

  return (
    <StyledBox
      {...(baseProps as any)}
      showCursor={!!(props.accessibilityRole === 'link' || !props.disabled)}
      sx={sx}
      onPress={handlePress}
    >
      {Platform.OS === 'web' && (
        <input
          {...radioProps}
          checked={props.checked ?? checked}
          readOnly
          type="radio"
          style={{
            height: 1,
            opacity: 0,
            overflow: 'hidden',
            position: 'absolute',
            width: 1,
            zIndex: -1,
          }}
        />
      )}
      {radioIcon({
        checked: props.checked ?? checked,
        color: sx.color as string,
        size: 24,
      })}
      {props.children}
    </StyledBox>
  );
};

export type { RadioProps };
export type { RadioGroupProps } from './RadioGroup';
export { RadioGroup } from './RadioGroup';
