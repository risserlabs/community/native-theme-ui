/**
 * File: /src/components/Radio/Radio.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 20-06-2022 07:09:30
 * Author: Lavanya Katari
 * -----
 * Last Modified: 09-09-2022 12:19:19
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { action } from '@storybook/addon-actions';
import React from 'react';
import { createArgsStory, createSxArgs, sxArgTypes, autoContrastArgType } from '../../../storybook';
import { Radio, RadioGroup } from './index';

export default {
  title: 'components/forms/Radio',
  component: Radio,
  parameters: {
    status: { type: 'beta' },
  },
};

export const radio = createArgsStory(Radio);
radio.args = {
  autoContrast: Radio.defaultProps?.autoContrast,
  checked: undefined,
  defaultChecked: true,
  children: undefined,
  disabled: false,
  ...createSxArgs(),
};
radio.argTypes = {
  hidden: { control: { type: 'boolean' } },
  disabled: { control: { type: 'boolean' } },
  children: { control: { type: 'text' } },
  defaultChecked: { control: { type: 'boolean' } },
  checked: { control: { type: 'boolean' } },
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const radioGroup = () => (
  <RadioGroup onChange={action('onChange')}>
    <Radio />
    <Radio defaultChecked />
    <Radio />
  </RadioGroup>
);
