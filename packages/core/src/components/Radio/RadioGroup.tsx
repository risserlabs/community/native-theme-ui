/**
 * File: /src/components/Radio/RadioGroup.tsx
 * Project: @native-theme-ui/core
 * File Created: 09-09-2022 07:55:56
 * Author: Clay Risser
 * -----
 * Last Modified: 09-09-2022 12:22:50
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ReactNode, createContext, useState, useEffect } from 'react';

type SetChecked = (checked: null | number) => void;

export const RadioGroupContext = createContext<[null | number, SetChecked]>([null, () => null]);

export const RadioIdContext = createContext<null | number>(null);

export interface RadioGroupProps {
  children?: ReactNode;
  onChange?: (index: number) => void;
}

export const RadioGroup: FC<RadioGroupProps> = (props: RadioGroupProps) => {
  const [radioGroupIndex, setRadioGroupIndex] = useState<null | number>(null);

  useEffect(() => {
    if (props.onChange && typeof radioGroupIndex === 'number') {
      props.onChange(radioGroupIndex);
    }
  }, [radioGroupIndex]);

  const children = (Array.isArray(props.children) ? props.children : [props.children]).map(
    (child: ReactNode, id: number) => (
      <RadioIdContext.Provider key={id} value={id}>
        {child}
      </RadioIdContext.Provider>
    ),
  );

  return (
    <RadioGroupContext.Provider value={[radioGroupIndex, setRadioGroupIndex]}>{children}</RadioGroupContext.Provider>
  );
};
