/**
 * File: /src/components/Donut/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:39:02
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { AutoContrast, useAutoContrast } from '@native-theme-ui/auto-contrast';
import { DripsyStyledProps, styled } from '@native-theme-ui/dripsy';
import { Svg, Circle, SvgProps } from 'react-native-svg';

const themeKey = 'variants';

const StyledSvg = styled(Svg, { themeKey })({});

export type DonutProps = DripsyStyledProps<SvgProps> & {
  autoContrast?: AutoContrast;
  max?: number;
  min?: number;
  size?: number;
  strokeWidth?: number | string;
  value?: number;
};

export const Donut: FC<DonutProps> = (props: DonutProps) => {
  const sx = useAutoContrast(props, {}, themeKey);
  const { strokeWidth, value, min, max } = props;

  const r = 16 - (typeof strokeWidth === 'number' ? strokeWidth : parseFloat(strokeWidth!));
  const C = 2 * r * Math.PI;
  const offset = C - ((value! - min!) / (max! - min!)) * C;

  return (
    <StyledSvg
      fill="none"
      height={props.size}
      stroke={sx.color as string}
      strokeWidth={strokeWidth}
      sx={sx}
      viewBox="0 0 32 32"
      width={props.size}
    >
      <Circle cx={16} cy={16} opacity={1 / 8} r={r} />
      <Circle cx={16} cy={16} r={r} strokeDasharray={C} strokeDashoffset={offset} transform="rotate(-90 16 16)" />
    </StyledSvg>
  );
};

Donut.defaultProps = {
  max: 1,
  min: 0,
  size: 128,
  strokeWidth: 2,
  value: 0,
};
