/**
 * File: /src/components/Card/Card.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 23-08-2022 06:33:41
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Card } from './index';
import { createArgsStory, createSxArgs, sxArgTypes, autoContrastArgType } from '../../../storybook';

export default {
  title: 'components/cards/Card',
  component: 'Card',
  parameters: {
    status: { type: 'beta' },
  },
};

export const card = createArgsStory(Card);
card.args = {
  children: 'I am a Card',
  autoContrast: undefined,
  ...createSxArgs(),
};
card.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const simpleCard = () => (
  <Card
    sx={{
      height: 300,
      width: 200,
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    Simple Card
  </Card>
);
