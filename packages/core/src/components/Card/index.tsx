/**
 * File: /src/components/Card/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 10:55:37
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Sx, styled, forwardThemeKey } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Box, BoxProps } from '../Box';

const themeKey = 'cards';

const defaultStyle: Sx = {
  bg: 'white',
  borderRadius: 6,
  overflow: 'visible',
  shadowColor: '#000',
  shadowOpacity: 0.2,
  shadowRadius: 8,
  elevation: 8,
  shadowOffset: {
    height: 0,
    width: 0,
  },
};

const StyledBox = styled(Box, {
  themeKey,
  defaultVariant: 'primary',
})(defaultStyle);

export type CardProps = BoxProps;

export const Card: FC<CardProps> = (props: CardProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  return <StyledBox {...(props as any)} sx={sx} themeKey={forwardThemeKey(props, themeKey)} />;
};
