/**
 * File: /src/components/AspectImage/AspectImage.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 23-06-2022 03:44:14
 * Author: K S R P BHUSHAN
 * -----
 * Last Modified: 05-09-2022 06:52:04
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { createArgsStory, createSxArgs, sxArgTypes } from '../../../storybook';
import { AspectImage } from './index';

export default {
  title: 'components/images/AspectImage',
  component: AspectImage,
  parameters: {
    status: { type: 'beta' },
  },
};

export const aspectImage = createArgsStory(AspectImage);
aspectImage.args = {
  src: 'https://images.unsplash.com/photo-1520222984843-df35ebc0f24d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9',
  ratio: 16 / 9,
  ...createSxArgs(),
};
aspectImage.argTypes = {
  ...sxArgTypes,
};

export const likeThemeUi = () => (
  <AspectImage
    ratio={4 / 3}
    src="https://images.unsplash.com/photo-1520222984843-df35ebc0f24d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9"
  />
);

export const animated = () => (
  <AspectImage
    fromSx={{
      scale: 0,
      rotate: '0deg',
    }}
    animateSx={{
      scale: 1,
      rotate: '360deg',
    }}
    transition={{ type: 'timing', duration: 2000 }}
    ratio={4 / 3}
    src="https://images.unsplash.com/photo-1520222984843-df35ebc0f24d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9"
  />
);
