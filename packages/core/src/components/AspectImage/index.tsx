/**
 * File: /src/components/AspectImage/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 23-06-2022 03:43:01
 * Author: K S R P BHUSHAN
 * -----
 * Last Modified: 27-12-2022 10:47:35
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { useCalculatedSx } from '@native-theme-ui/dripsy';
import { AspectImageProps, useSplitProps } from './props';
import { AspectRatio } from '../AspectRatio';
import { Image } from '../Image';

export const AspectImage: FC<AspectImageProps> = (props: AspectImageProps) => {
  const sx = useCalculatedSx(props.sx);
  const { aspectRatioProps, aspectRatioStyle, aspectRatioSx, baseProps, baseStyle, baseSx, sharedStyle, sharedSx } =
    useSplitProps(props, {
      objectFit: 'cover',
      ...sx,
    });

  return (
    <AspectRatio
      {...aspectRatioProps}
      sx={{ ...sharedSx, ...aspectRatioSx }}
      style={{ ...sharedStyle, ...aspectRatioStyle }}
    >
      <Image {...baseProps} sx={{ ...sharedSx, ...baseSx }} style={{ ...sharedStyle, ...baseStyle }} />
    </AspectRatio>
  );
};

export type { AspectImageProps };
