/**
 * File: /src/components/AspectImage/props.ts
 * Project: @native-theme-ui/core
 * File Created: 20-07-2022 23:39:41
 * Author: Harikittu46
 * -----
 * Last Modified: 27-12-2022 10:47:46
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Sx } from '@native-theme-ui/dripsy';
import { ImageProps } from '../Image';
import { createUseSplitProps, splitPropsGroups } from '../../util';

export type BasePropsBucket = ImageProps;

export interface AspectRatioPropsBucket {
  ratio?: number;
}

export const useSplitProps = createUseSplitProps<
  AspectImageProps,
  {
    baseProps: BasePropsBucket;
    aspectRatioProps: AspectRatioPropsBucket;
  },
  {
    baseSx: Sx;
    aspectRatioSx: Sx;
    sharedSx: Sx;
  },
  {
    baseStyle: Record<string, unknown>;
    aspectRatioStyle: Record<string, unknown>;
    sharedStyle: Record<string, unknown>;
  }
>(
  {
    aspectRatioProps: ['ratio', ...splitPropsGroups.props.moti, ...splitPropsGroups.props.pressable],
  },
  {
    aspectRatioSx: [...splitPropsGroups.sx.margin],
    sharedSx: ['width', 'height'],
  },
);

export type AspectImageProps = Omit<BasePropsBucket & AspectRatioPropsBucket, 'children'>;
