/**
 * File: /src/components/Divider/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 10:59:45
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Sx, styled } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Box, BoxProps } from '../Box';

const themeKey = 'variants';

const defaultStyle: Sx = {
  border: 0,
  borderBottomStyle: 'solid',
  borderBottomWidth: 1,
  borderColor: 'gray',
  my: 2,
};

const StyledBox = styled(Box, {
  themeKey,
  defaultVariant: 'styles.hr',
})(defaultStyle);

type StyledBoxProps = BoxProps;

export type DividerProps = Omit<StyledBoxProps, 'children'>;

export const Divider: FC<DividerProps> = (props: DividerProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return <StyledBox {...(props as any)} sx={sx} />;
};
