/**
 * File: /src/components/Divider/Divider.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 05-09-2022 07:10:45
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { createArgsStory, createSxArgs, sxArgTypes, autoContrastArgType } from '../../../storybook';
import { Box } from '../Box';
import { Divider } from './index';

export default {
  title: 'components/Divider',
  component: Divider,
  parameters: {
    status: { type: 'beta' },
  },
};

export const divider = createArgsStory(Divider);
divider.args = {
  autoContrast: undefined,
  ...createSxArgs(),
};
divider.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const withBackGround = () => (
  <Box sx={{ bg: 'background', p: 4 }}>
    <Divider autoContrast="AAA" />
  </Box>
);

export const animated = () => (
  <Box sx={{ p: 4 }}>
    <Divider
      autoContrast="AAA"
      sx={{ height: 4 }}
      fromSx={{ scale: 0, transform: [{ skewX: '20deg' }, { skewY: '20deg' }] }}
      animateSx={{
        scale: 1,
        transform: [{ skewX: '0deg' }, { skewY: '0deg' }],
      }}
      transition={{ type: 'timing', duration: 2000 }}
    />
  </Box>
);
