/**
 * File: /src/components/Image/props.ts
 * Project: @native-theme-ui/core
 * File Created: 20-07-2022 23:39:41
 * Author: Harikittu46
 * -----
 * Last Modified: 27-12-2022 11:41:18
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ImageSourcePropType, ImageProps as RNImageProps } from 'react-native';
import { Sx, DripsyStyledProps } from '@native-theme-ui/dripsy';
import { Asset } from 'expo-asset';
import { BoxProps } from '../Box';
import { createUseSplitProps, splitPropsGroups } from '../../util';

export type BasePropsBucket = Omit<DripsyStyledProps<RNImageProps>, 'source'> & {
  src?: string | ImageSourcePropType | Asset;
  svg?: boolean;
};

export type MotiPressablePropsBucket = BoxProps;

export const useSplitProps = createUseSplitProps<
  ImageProps,
  {
    baseProps: BasePropsBucket;
    motiPressableProps: MotiPressablePropsBucket;
  },
  {
    baseSx: Sx;
    innerSx: Sx;
  },
  {
    baseStyle: Record<string, unknown>;
    innerStyle: Record<string, unknown>;
  }
>(
  {
    motiPressableProps: [...splitPropsGroups.props.moti, ...splitPropsGroups.props.pressable],
  },
  {
    innerSx: splitPropsGroups.sx.inner,
  },
);

export type ImageProps = Omit<BasePropsBucket & MotiPressablePropsBucket, 'children'>;
