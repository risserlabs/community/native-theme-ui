/**
 * File: /src/components/Image/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:05:11
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ComponentProps, useState, useEffect } from 'react';
import { Asset } from 'expo-asset';
import { SvgUri } from 'react-native-svg';
import { Sx, styled } from '@native-theme-ui/dripsy';
import { XMLParser } from 'fast-xml-parser';
import {
  Image as RNImage,
  ImageResizeMode,
  ImageSourcePropType,
  ImageStyle,
  ImageURISource,
  Platform,
  StyleProp,
  StyleSheet,
} from 'react-native';
import { useCalculatedSx } from '@native-theme-ui/dripsy';
import { Box } from '../Box';
import { ImageProps, useSplitProps } from './props';
import { bottomStyle } from '../../util';

const logger = console;

const defaultStyle: Sx = {
  height: 'auto',
  maxWidth: '100%',
  width: '100%',
};

const StyledImage = styled(RNImage, { themeKey: 'images' })(defaultStyle);

const StyledSvgUri = styled(SvgUri, { themeKey: 'images' })(defaultStyle);

const StyledOuterBox = styled(Box)({
  position: 'relative',
  overflow: 'hidden',
});

const parser = new XMLParser({
  ignoreAttributes: false,
  attributeNamePrefix: '@_',
  allowBooleanAttributes: true,
});

export const Image: FC<ImageProps> = (props: ImageProps) => {
  const [imageRatio, setImageRatio] = useState<number>();
  const [svgViewBox, setSvgViewBox] = useState<SvgViewBox>();
  const sx = useCalculatedSx({ ...props.sx });
  const style = { ...(StyleSheet.flatten(props.style || {}) as ImageStyle) };
  style.resizeMode = getResizeMode(sx, (style as { objectFit?: string }).objectFit, style.resizeMode);
  delete (style as { objectFit?: string }).objectFit;
  delete sx.objectFit;
  const { baseProps, baseStyle, baseSx, innerStyle, innerSx, motiPressableProps } = useSplitProps(props, sx);
  const isMotiPressable = !!Object.keys(motiPressableProps).length;
  let { svg } = props;

  const styledImageProps = { ...baseProps } as ComponentProps<typeof StyledImage> & {
    src?: string | ImageSourcePropType;
  };
  let source = props.src as ImageSourcePropType;
  if (typeof props.src === 'string') {
    source = { uri: props.src };
    if (props.src.substring(props.src.length - 4) === '.svg' && typeof svg === 'undefined') {
      svg = true;
    }
  } else if ((props.src as Asset)?.localUri && (props.src as Asset)?.type) {
    source = {
      uri: (props.src as Asset).uri,
    };
    if ((props.src as Asset).type === 'svg' && typeof svg === 'undefined') {
      svg = true;
    }
  }
  delete styledImageProps.src;
  const uri = typeof source === 'number' ? source : (source as ImageURISource)?.uri;

  const calculateHeight =
    style.height === 'auto' ||
    (typeof style.height === 'undefined' && (typeof sx.height === 'undefined' || sx.height === 'auto'));

  useEffect(() => {
    if (Platform.OS !== 'web' && svg) {
      (async () => {
        if (!imageRatio || !svgViewBox) {
          const { height, width, viewBox } = await getSvgMeta(uri);
          if (typeof height === 'number' && typeof width === 'number' && viewBox) {
            setSvgViewBox(viewBox);
            const ratio = width / height;
            setImageRatio(ratio);
          }
        }
      })();
    } else if (typeof uri !== 'number' && uri && !imageRatio && calculateHeight) {
      RNImage.getSize(uri, (width: number, height: number) => {
        const ratio = width / height;
        if (Number.isFinite(ratio)) setImageRatio(ratio);
      });
    }
  }, [uri, imageRatio, calculateHeight]);

  function renderNativeSvg(
    props: any,
    { innerStyle, innerSx, outerStyle, outerSx }: RenderImageStyles = {
      innerStyle: {},
      innerSx: {},
      outerStyle: {},
      outerSx: {},
    },
  ) {
    const relativeSize = calculateSize(svgViewBox?.[2], svgViewBox?.[3], imageRatio);
    const viewBox =
      svgViewBox && relativeSize
        ? [svgViewBox[0], svgViewBox[1], relativeSize[0], relativeSize[1]].join(' ')
        : undefined;
    const width = style.width || style.width === 0 ? style.width : sx.width || sx.width;
    const height = style.height || style.height === 0 ? style.height : sx.height;
    const ready = !!viewBox;
    return (
      <StyledOuterBox
        sx={outerSx}
        style={{
          ...outerStyle,
          ...(height ? { height } : {}),
          ...(width ? { width } : {}),
        }}
      >
        <StyledSvgUri
          {...props}
          sx={{ ...innerSx, ...outerSx }}
          style={{
            innerStyle,
            ...bottomStyle,
            ...outerStyle,
            display: ready ? style.display : 'none',
          }}
          width="100%"
          height="100%"
          preserveAspectRatio={getPreserveAspectRatio(style.resizeMode)}
          uri={uri}
          {...(viewBox ? { viewBox } : {})}
        />
      </StyledOuterBox>
    );
  }

  function renderImage(
    props: any,
    { innerStyle, innerSx, outerStyle, outerSx }: RenderImageStyles = {
      innerStyle: {},
      innerSx: {},
      outerStyle: {},
      outerSx: {},
    },
  ) {
    if (!props.src || !uri) <Box {...props} />;
    if (svg && Platform.OS !== 'web') return renderNativeSvg(props, { innerStyle, outerStyle, innerSx, outerSx });
    return (
      <StyledImage
        {...props}
        source={source}
        style={{ resizeMode: style.resizeMode, ...innerStyle, ...outerStyle }}
        sx={{ ...innerSx, ...outerSx }}
      />
    );
  }

  if (calculateHeight && imageRatio) {
    return (
      <StyledOuterBox {...(motiPressableProps as any)} sx={baseSx} style={baseStyle}>
        <Box
          style={{
            height: 0,
            paddingBottom: `${100 / imageRatio}%`,
            width: '100%',
          }}
        />
        {renderImage(
          {
            ...styledImageProps,
          },
          {
            innerSx,
            innerStyle,
            outerStyle: {
              bottom: 0,
              left: 0,
              position: 'absolute',
              right: 0,
              top: 0,
            },
          },
        )}
      </StyledOuterBox>
    );
  }

  if (isMotiPressable) {
    return (
      <StyledOuterBox {...(motiPressableProps as any)} sx={baseSx} style={{ ...baseStyle }}>
        {renderImage(
          {
            ...styledImageProps,
          },
          {
            innerStyle,
            innerSx,
            outerStyle: {
              overflow: 'visible',
              width: '100%',
              height: '100%',
            },
          },
        )}
      </StyledOuterBox>
    );
  }

  return renderImage(styledImageProps, {
    innerStyle,
    innerSx,
    outerStyle: baseStyle,
    outerSx: baseSx,
  });
};

export type { ImageProps };

export interface SvgMeta {
  height?: number;
  width?: number;
  viewBox?: SvgViewBox;
}

type Width = number;
type Height = number;
export type SvgViewBox = [number, number, Width, Height];

interface RenderImageStyles {
  outerStyle?: StyleProp<any>;
  innerStyle?: StyleProp<any>;
  outerSx?: Sx;
  innerSx?: Sx;
}

function calculateSize(width?: number, height?: number, ratio?: number): [Width, Height] | undefined {
  if (typeof width === 'number' && Number.isFinite(width) && typeof height === 'number' && Number.isFinite(height)) {
    return [width, height];
  }
  if (typeof width === 'number' && Number.isFinite(width) && typeof ratio === 'number' && Number.isFinite(ratio)) {
    const height = width / ratio;
    if (typeof height !== 'number' || !Number.isFinite(height)) {
      return;
    }
    return [width, height];
  }
  if (typeof height === 'number' && Number.isFinite(height) && typeof ratio === 'number' && Number.isFinite(ratio)) {
    const width = height / ratio;
    if (typeof width !== 'number' || !Number.isFinite(width)) {
      return;
    }
    return [width, height];
  }
  return;
}

async function getSvgMeta(uri?: string | number) {
  const meta: SvgMeta = {};
  if (Platform.OS === 'web' || typeof uri === 'number' || !uri) return meta;
  const res = await fetch(uri);
  try {
    const xml = parser.parse(await res.text());
    const width = parseInt(xml?.svg?.['@_width'], 10);
    const height = parseInt(xml?.svg?.['@_height'], 10);
    const viewBox = xml?.svg?.['@_viewBox']
      ?.toString()
      .split(' ')
      .reduce(
        (viewBox: SvgViewBox | undefined, itemStr: string, i: number) => {
          if (!viewBox) return {};
          const itemNum = parseInt(itemStr, 10);
          if (!Number.isFinite(itemNum)) return {};
          viewBox[i] = itemNum;
          return viewBox;
        },
        [0, 0, 0, 0],
      ) as SvgViewBox;
    if (typeof width === 'number' && Number.isFinite(width)) meta.width = width;
    if (typeof height === 'number' && Number.isFinite(height)) meta.height = height;
    if (viewBox) meta.viewBox = viewBox;
    return meta;
  } catch (err) {
    logger.error(err);
  }
  return meta;
}

function getResizeMode(sx: Sx, objectFit?: string, resizeMode?: ImageResizeMode): ImageResizeMode {
  if (resizeMode) return resizeMode;
  if (objectFit === 'fill') return 'stretch';
  if (objectFit === 'cover' || objectFit === 'contain') return objectFit;
  if (sx.objectFit === 'fill') return 'stretch';
  if (sx.objectFit === 'cover' || sx.objectFit === 'contain') {
    return sx.objectFit;
  }
  return 'stretch';
}

function getPreserveAspectRatio(resizeMode?: ImageResizeMode): string {
  switch (resizeMode) {
    case 'stretch': {
      return 'none';
    }
    case 'cover': {
      return 'xMidYMid slice';
    }
  }
  return 'xMidYMid meet';
}
