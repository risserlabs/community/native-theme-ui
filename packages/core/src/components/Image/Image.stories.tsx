/**
 * File: /src/components/Image/Image.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 11-11-2022 14:57:42
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* eslint-disable @typescript-eslint/no-explicit-any */

import React from 'react';
import { useAssets } from 'expo-asset';
import { Image } from './index';
import { createArgsStory, createSxArgs, sxArgTypes } from '../../../storybook';
import { Box } from '../Box';
import { Args } from '@storybook/addons';

export default {
  title: 'components/images/Image',
  component: Image,
  parameters: {
    status: { type: 'beta' },
  },
};

export const image = createArgsStory(Image);
image.args = {
  src: 'https://images.unsplash.com/photo-1520222984843-df35ebc0f24d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9',
  sxObjectFit: '',
  ...createSxArgs(),
};
image.argTypes = {
  ...sxArgTypes,
  sxObjectFit: {
    options: ['fill', 'cover', 'contain', ''],
    control: { type: 'select' },
  },
};

export const likeThemeUi = () => (
  <Image src="https://images.unsplash.com/photo-1520222984843-df35ebc0f24d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9" />
);

export const styledImage = () => (
  <Image
    src="https://images.unsplash.com/photo-1520222984843-df35ebc0f24d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9"
    sx={{
      borderColor: 'primary',
      borderRadius: 150 / 2,
      borderWidth: 3,
      height: 150,
      objectFit: 'fill',
      overflow: 'hidden',
      width: 150,
    }}
  />
);

const Svg = (args: Args) => {
  const [assets] = useAssets([require('../../../assets/shape.svg')]);
  if (!assets) return null;
  return (
    <Image
      src={assets[0]}
      sx={{ width: args.width, height: args.height, objectFit: args.objectFit, borderRadius: args.borderRadius }}
    />
  );
};
export const svg = (args: any) => {
  return <Svg {...args} />;
};
svg.args = {
  objectFit: 'contain',
  // width: 300,
  borderRadius: 0,
  // height: undefined,
};

export const animated = () => {
  return (
    <Image
      src="https://images.unsplash.com/photo-1520222984843-df35ebc0f24d?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjF9"
      fromSx={{
        rotate: '0deg',
        scale: 0,
      }}
      animateSx={{
        rotate: '360deg',
        scale: 1,
      }}
      sx={{
        height: 200,
        objectFit: 'cover',
      }}
      transition={{ type: 'timing', duration: 3000 }}
    />
  );
};

export const CircleSvg = () => {
  const [assets] = useAssets([require('../../../assets/shape.svg')]);
  if (!assets) return null;
  return (
    <Box
      sx={{
        alignItems: 'center',
        bg: 'primary',
        display: 'flex',
        height: 200,
        justifyContent: 'center',
        width: 200,
      }}
    >
      <Image
        src={assets[0]}
        sx={{
          bg: 'white',
          borderColor: 'secondary',
          borderRadius: 9999,
          borderWidth: 8,
          height: 100,
          objectFit: 'cover',
          width: 100,
        }}
      />
    </Box>
  );
};
export const circleSvg = (args: Args) => <CircleSvg {...args} />;
