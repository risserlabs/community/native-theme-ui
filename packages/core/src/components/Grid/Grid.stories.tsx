/**
 * File: /src/components/Grid/Grid.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 27-06-2022 10:04:09
 * Author: Ajithkrm6
 * -----
 * Last Modified: 27-12-2022 11:28:12
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Grid } from './index';
import { Box } from '../Box';
import { createArgsStory, createSxArgs, sxArgTypes, autoContrastArgType } from '../../../storybook';

export default {
  title: 'components/grids/Grid',
  component: Grid,
  parameters: {
    type: { status: 'beta' },
  },
};

export const grid = createArgsStory(Grid);
grid.args = {
  children: [
    <Box key="0" sx={{ bg: 'primary' }}>
      Box1
    </Box>,
    <Box key="1" sx={{ bg: 'secondary' }}>
      Box2
    </Box>,
    <Box key="2" sx={{ bg: 'muted' }}>
      Box3
    </Box>,
    <Box key="3" sx={{ bg: 'primary' }}>
      Box4
    </Box>,
    <Box key="4" sx={{ bg: 'secondary' }}>
      Box2
    </Box>,
    <Box key="5" sx={{ bg: 'muted' }}>
      Box6
    </Box>,
    <Box key="6" sx={{ bg: 'primary' }}>
      Box7
    </Box>,
    <Box key="7" sx={{ bg: 'secondary' }}>
      Box8
    </Box>,
    <Box key="8" sx={{ bg: 'muted' }}>
      Box9
    </Box>,
  ],
  columns: 3,
  ...createSxArgs(),
};
grid.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const likeThemeUi = () => (
  <Grid>
    <Box sx={{ bg: 'primary' }}>Box1</Box>
    <Box sx={{ bg: 'secondary' }}>Box2</Box>
    <Box sx={{ bg: 'muted' }}>Box3</Box>
    <Box sx={{ bg: 'primary' }}>Box4</Box>
    <Box sx={{ bg: 'secondary' }}>Box2</Box>
    <Box sx={{ bg: 'muted' }}>Box6</Box>
    <Box sx={{ bg: 'primary' }}>Box7</Box>
    <Box sx={{ bg: 'secondary' }}>Box8</Box>
    <Box sx={{ bg: 'muted' }}>Box9</Box>
  </Grid>
);

export const animated = () => (
  <Grid
    fromSx={{
      transform: [{ skewX: '0deg' }, { skewY: '0deg' }],
    }}
    animateSx={{
      transform: [{ skewX: '360deg' }, { skewY: '360deg' }],
    }}
    transition={{ type: 'timing', duration: 5000 }}
    columns={3}
  >
    <Box sx={{ bg: 'primary' }}>Box1</Box>
    <Box sx={{ bg: 'secondary' }}>Box2</Box>
    <Box sx={{ bg: 'muted' }}>Box3</Box>
    <Box sx={{ bg: 'primary' }}>Box4</Box>
    <Box sx={{ bg: 'secondary' }}>Box2</Box>
    <Box sx={{ bg: 'muted' }}>Box6</Box>
    <Box sx={{ bg: 'primary' }}>Box7</Box>
    <Box sx={{ bg: 'secondary' }}>Box8</Box>
    <Box sx={{ bg: 'muted' }}>Box9</Box>
  </Grid>
);
