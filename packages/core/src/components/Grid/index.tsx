/**
 * File: /src/components/Grid/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 27-06-2022 10:03:43
 * Author: Ajithkrm6
 * -----
 * Last Modified: 27-12-2022 12:37:28
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ReactNode } from 'react';
import { FlatList, ListRenderItem, View, ListRenderItemInfo } from 'react-native';
import { Sx, useBreakpointIndex, styled } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Box } from '../Box';
import { useSplitProps, GridProps } from './props';

const themeKey = 'grids';

const defaultStyle: Sx = {
  width: '100%',
};

const StyledBox = styled(Box, { themeKey })(defaultStyle);

export interface DataItem {
  node: ReactNode;
  columns: number;
}

const Item: ListRenderItem<DataItem> = ({ item }: ListRenderItemInfo<DataItem>) => {
  return (
    <View
      style={{
        flex: 1,
        maxWidth: `${100 / item.columns}%`,
      }}
    >
      <Box style={{ width: '100%' }}>{item.node}</Box>
    </View>
  );
};

export const Grid: FC<GridProps> = (props: GridProps) => {
  const breakpointIndex = useBreakpointIndex();
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  const { baseProps, flatListProps } = useSplitProps(props, sx);
  let columns = typeof props.columns === 'number' ? props.columns : 4;
  if (Array.isArray(props.columns) && breakpointIndex < props.columns.length) {
    columns = props.columns[breakpointIndex];
  }

  const data: DataItem[] = (Array.isArray(props.children) ? props.children : [props.children]).map(
    (node: ReactNode) => ({ node, columns }),
  );

  return (
    <StyledBox
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      {...(baseProps as any)}
      sx={sx}
      style={{
        flex: columns,
      }}
    >
      <FlatList {...flatListProps} key={columns} data={data} numColumns={columns} renderItem={Item} />
    </StyledBox>
  );
};

export type { GridProps };
