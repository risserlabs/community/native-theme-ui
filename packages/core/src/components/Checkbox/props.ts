/**
 * File: /src/components/Checkbox/props.ts
 * Project: @native-theme-ui/core
 * File Created: 05-07-2022 06:24:30
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 10:57:28
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Sx } from '@native-theme-ui/dripsy';
import { StyledBoxProps } from './index';
import { createUseSplitProps, splitPropsGroups } from '../../util';

export type BasePropsBucket = StyledBoxProps;

export interface CheckboxPropsBucket {
  checked?: boolean;
  disabled?: boolean;
  name?: string;
}

export interface CustomPropsBucket {
  defaultChecked?: boolean;
}

export const useSplitProps = createUseSplitProps<
  CheckboxProps,
  {
    baseProps: BasePropsBucket;
    checkboxProps: CheckboxPropsBucket;
    customProps: CustomPropsBucket;
  },
  {
    baseSx: Sx;
    voidSx: Sx;
  },
  {
    baseStyle: Record<string, unknown>;
    voidStyle: Record<string, unknown>;
  }
>(
  {
    checkboxProps: ['checked', 'disabled', 'name'],
    customProps: ['defaultChecked'],
  },
  {
    voidSx: splitPropsGroups.sx.padding,
  },
);

export type CheckboxProps = BasePropsBucket & CheckboxPropsBucket & CustomPropsBucket;
