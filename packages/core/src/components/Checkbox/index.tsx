/**
 * File: /src/components/Checkbox/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 17-06-2022 01:05:11
 * Author: Lavanya Katari
 * -----
 * Last Modified: 27-12-2022 10:57:13
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, useState, useCallback } from 'react';
import { Platform } from 'react-native';
import { Svg, Path, SvgProps } from 'react-native-svg';
import { Sx, styled } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Box, BoxProps } from '../Box';
import { useSplitProps, CheckboxProps } from './props';

const themeKey = 'forms';

const defaultStyle: Sx = {};

const checkboxChecked = (props: SvgProps & { size: number }) => (
  <Svg width={props.size} height={props.size} viewBox="0 0 24 24" fill="currentColor" {...props}>
    {/* eslint-disable-next-line spellcheck/spell-checker */}
    <Path d="M19 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.11 0 2-.9 2-2V5c0-1.1-.89-2-2-2zm-9 14l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z" />
  </Svg>
);

const checkboxUnchecked = (props: SvgProps & { size: number }) => (
  <Svg width={props.size} height={props.size} viewBox="0 0 24 24" fill="currentColor" {...props}>
    <Path d="M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z" />
  </Svg>
);

const checkboxIcon = (props: SvgProps & { size: number; checked: boolean }) => {
  if (props.checked) return checkboxChecked(props);
  return checkboxUnchecked(props);
};

const StyledBox = styled(Box, {
  themeKey,
  defaultVariant: 'radio',
})(({ showCursor }: { showCursor: boolean }) => ({
  ...Platform.select({
    web: {
      ...(showCursor ? { cursor: 'pointer' } : {}),
    },
  }),
  ...defaultStyle,
}));

export type StyledBoxProps = BoxProps;

export const Checkbox: FC<CheckboxProps> = (props: CheckboxProps) => {
  const [checked, setChecked] = useState(props.defaultChecked ?? false);
  const sx = useAutoContrast(
    props,
    {
      ...defaultStyle,
      color: props.disabled ? 'gray' : 'primary',
    },
    themeKey,
  );
  const { baseProps, checkboxProps } = useSplitProps(props, sx);

  const handlePress = useCallback(() => {
    if (props.disabled) return;
    setChecked((checked: boolean) => !checked);
    if (props.onPress) props.onPress();
  }, [props.disabled]);

  return (
    <StyledBox
      {...(baseProps as any)}
      showCursor={!!(props.accessibilityRole === 'link' || !props.disabled)}
      sx={sx}
      onPress={handlePress}
    >
      {Platform.OS === 'web' && (
        <input
          {...checkboxProps}
          checked={props.checked ?? checked}
          readOnly
          type="checkbox"
          style={{
            height: 1,
            opacity: 0,
            overflow: 'hidden',
            position: 'absolute',
            width: 1,
            zIndex: -1,
          }}
        />
      )}
      {checkboxIcon({
        checked: props.checked ?? checked,
        color: sx.color as string,
        size: 24,
        style: { borderRadius: 4 },
      })}
      {props.children}
    </StyledBox>
  );
};

export type { CheckboxProps };
