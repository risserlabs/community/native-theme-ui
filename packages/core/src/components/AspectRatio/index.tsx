/**
 * File: /src/components/AspectRatio/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 27-06-2022 03:42:37
 * Author: Harikittu46
 * -----
 * Last Modified: 27-12-2022 10:48:28
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { styled } from '@native-theme-ui/dripsy';
import { AspectRatioProps, useSplitProps } from './props';
import { Box } from '../Box';

const StyledOuter = styled(Box)({
  position: 'relative',
  overflow: 'hidden',
});

const StyledInner = styled(Box)({
  position: 'absolute',
  bottom: 0,
  left: 0,
  right: 0,
  top: 0,
});

export const AspectRatio: FC<AspectRatioProps> = (props: AspectRatioProps) => {
  const { baseProps, baseSx, outerSx, baseStyle, outerStyle, outerProps } = useSplitProps(props);
  return (
    <StyledOuter {...(outerProps as any)} sx={outerSx} style={outerStyle}>
      <Box
        style={{
          width: '100%',
          height: 0,
          paddingBottom: `${100 / (props.ratio || 1)}%`,
        }}
      />
      <StyledInner {...(baseProps as any)} sx={baseSx} style={baseStyle} />
    </StyledOuter>
  );
};

export type { AspectRatioProps };
