/**
 * File: /src/components/AspectRatio/props.ts
 * Project: @native-theme-ui/core
 * File Created: 20-07-2022 23:39:41
 * Author: Harikittu46
 * -----
 * Last Modified: 27-12-2022 12:41:19
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Sx } from '@native-theme-ui/dripsy';
import { createUseSplitProps, splitPropsGroups } from '../../util';
import { BoxProps } from '../Box';

export type BasePropsBucket = BoxProps;

export interface CustomPropsBucket {
  ratio?: number;
}

export const useSplitProps = createUseSplitProps<
  AspectRatioProps,
  {
    baseProps: BasePropsBucket;
    customProps: CustomPropsBucket;
    outerProps: BoxProps;
  },
  {
    baseSx: Sx;
    outerSx: Sx;
  },
  {
    baseStyle: Record<string, unknown>;
    outerStyle: Record<string, unknown>;
  }
>(
  {
    customProps: ['ratio'],
    outerProps: [...splitPropsGroups.props.moti, ...splitPropsGroups.props.pressable],
  },
  {
    outerSx: ['height', 'width', ...splitPropsGroups.sx.margin],
  },
);

export type AspectRatioProps = BasePropsBucket & CustomPropsBucket;
