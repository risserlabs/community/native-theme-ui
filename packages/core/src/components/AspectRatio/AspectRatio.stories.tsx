/**
 * File: /src/components/AspectRatio/AspectRatio.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 27-06-2022 03:42:55
 * Author: Harikittu46
 * -----
 * Last Modified: 23-08-2022 09:31:44
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { autoContrastArgType, createArgsStory, createSxArgs, sxArgTypes } from '../../../storybook';
import { AspectRatio } from './index';
import { Heading } from '../Heading';

export default {
  title: 'components/variants/AspectRatio',
  component: AspectRatio,
  parameters: {
    status: { type: 'beta' },
  },
};

export const aspectRatio = createArgsStory(AspectRatio);
aspectRatio.args = {
  ratio: 4 / 3,
  autoContrast: undefined,
  ...createSxArgs({ bg: 'primary' }),
};
aspectRatio.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const likeThemeUi = () => (
  <AspectRatio
    ratio={16 / 9}
    sx={{
      p: 4,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: 'background',
      bg: 'primary',
    }}
  >
    <Heading>Aspect Ratio</Heading>
  </AspectRatio>
);
