/**
 * File: /src/components/Spinner/Spinner.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 07:30:17
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Box } from '../../components/Box';
import { Spinner } from './index';
import { createArgsStory, createSxArgs, sxArgTypes, autoContrastArgType } from '../../../storybook';

export default {
  title: 'components/Spinner',
  component: Spinner,
  parameters: {
    status: { type: 'beta' },
  },
};

export const spinner = createArgsStory(Spinner);
spinner.args = {
  duration: 500,
  size: 48,
  strokeWidth: 4,
  autoContrast: undefined,
  ...createSxArgs(),
};
spinner.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const likeThemeUi = () => <Spinner sx={{ color: 'primary' }} />;

export const withBackGround = () => (
  <Box sx={{ bg: 'background', p: 4 }}>
    <Spinner />
  </Box>
);
