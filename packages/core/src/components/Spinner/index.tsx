/**
 * File: /src/components/Spinner/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:31:45
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Easing } from 'react-native-reanimated';
import { StyleProp, ViewStyle } from 'react-native';
import { Svg, Circle, SvgProps } from 'react-native-svg';
import { styled, Sx } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Box, BoxProps } from '../Box';

const themeKey = 'variants';

const defaultStyle: Sx = {
  color: 'primary',
  overflow: 'visible',
};

const defaultBoxStyle: Sx = {
  justifyContent: 'center',
  alignItems: 'center',
};

const StyledSvg = styled(Svg, {
  themeKey,
})(defaultStyle);

const StyledBox = styled(Box, {
  themeKey,
})(({ size }: { size: number }) => ({
  ...defaultStyle,
  ...defaultBoxStyle,
  width: size,
  height: size,
}));

export type SpinnerProps = BoxProps & {
  duration?: number;
  size?: number;
  strokeWidth?: number;
};

export const Spinner: FC<SpinnerProps> = (props: SpinnerProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  const r = 16 - props.strokeWidth!;
  const C = 2 * r * Math.PI;
  const offset = C - (1 / 4) * C;

  const svgProps: SvgProps = {
    fill: 'none',
    height: props.size,
    stroke: 'currentColor',
    strokeWidth: props.strokeWidth,
    viewBox: '0 0 32 32',
    width: props.size,
    style: { color: sx.color } as StyleProp<ViewStyle>,
  };

  return (
    <StyledBox {...(props as any)} sx={sx}>
      <StyledSvg {...svgProps} style={{ opacity: 1 / 8 }}>
        <Circle cx={16} cy={16} r={r} />
      </StyledSvg>
      <Box
        from={{ rotate: '0deg' }}
        animate={{ rotate: '360deg' }}
        transition={{
          duration: props.duration,
          easing: Easing.linear,
          loop: true,
          repeatReverse: false,
          type: 'timing',
        }}
        style={{
          borderRadius: 9999,
          overflow: 'hidden',
          position: 'absolute',
        }}
      >
        <StyledSvg {...svgProps}>
          <Circle cx={16} cy={16} r={r} strokeDasharray={C} strokeDashoffset={offset} />
        </StyledSvg>
      </Box>
    </StyledBox>
  );
};

Spinner.defaultProps = {
  duration: 500,
  size: 48,
  strokeWidth: 4,
};
