/**
 * File: /src/components/Button/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 17-06-2022 07:34:18
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:30:44
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { ComponentPropsWithRef, forwardRef, PropsWithRef } from 'react';
import { BackgroundColorProvider } from '@native-theme-ui/auto-contrast';
import { MotiPressable } from 'moti/interactions';
import { MotiView } from 'moti';
import { Platform, Pressable as RNPressable, View } from 'react-native';
import { RefAttributes } from 'react';
import { Sx, styled, useSx } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { ButtonProps as Props, useSplitProps } from './props';
import { Text } from '../Text';
import { bottomStyle, isText, passthroughStyle } from '../../util';
import { useCalculateMoti } from '../../hooks';

const themeKey = 'buttons';

const defaultOuterStyle: Record<string, unknown> = {
  appearance: 'none',
  bg: 'primary',
  border: 0,
  borderRadius: 4,
  userSelect: 'none',
};

const defaultTextStyle: Sx = {
  color: 'white',
  textAlign: 'center',
};

const defaultInnerStyle: Sx = {
  boxSizing: 'border-box',
  display: 'flex',
  flexDirection: 'column',
  px: 3,
  py: 2,
  flexShrink: 1,
  borderRadius: 4,
};

const StyledMotiPressable = styled(MotiPressable, {
  themeKey,
  defaultVariant: 'primary',
})(defaultInnerStyle);

const StyledMotiView = styled(MotiView, {
  themeKey,
  defaultVariant: 'primary',
})(defaultInnerStyle);

const StyledText = styled(Text, { themeKey, defaultVariant: 'primary' })(defaultTextStyle);

export const Button = forwardRef((props: Props, ref?: ComponentPropsWithRef<typeof RNPressable>['ref']) => {
  const sx = useAutoContrast(
    props,
    {
      ...defaultOuterStyle,
      ...defaultTextStyle,
    },
    themeKey,
  );
  const calcSx = useSx();
  const {
    baseProps,
    baseStyle,
    baseSx,
    innerStyle,
    innerSx,
    motiPressableOuterStyle,
    motiPressableOuterSx,
    motiProps,
    sharedStyle,
    sharedSx,
    textProps,
    textStyle,
    textSx,
  } = useSplitProps(props, {
    display: props.hidden ? 'none' : 'flex',
    ...sx,
  });
  const { animate, from, exit } = useCalculateMoti(props, themeKey);
  const isMoti = !!Object.keys(motiProps).length;
  const platformStyle = Platform.select({
    web: {
      ...(props.onPress || props.accessibilityRole === 'link' || !props.disabled
        ? {
            cursor: 'pointer',
          }
        : {}),
    },
  });

  const children = isText(props.children) ? (
    <StyledText {...(textProps as any)} sx={textSx} style={textStyle}>
      {props.children}
    </StyledText>
  ) : (
    props.children
  );

  const renderChildren = () => (
    <BackgroundColorProvider {...props} themeKey={themeKey} defaultStyle={defaultOuterStyle}>
      {children}
    </BackgroundColorProvider>
  );

  if (isMoti) {
    const outerStyle = {
      ...calcSx(defaultOuterStyle || {}, { themeKey }),
      ...calcSx(motiPressableOuterSx || {}, { themeKey }),
      ...calcSx(sharedSx || {}, { themeKey }),
      ...motiPressableOuterStyle,
      ...sharedStyle,
      ...platformStyle,
    };
    return (
      <MotiPressable
        {...(baseProps as any)}
        containerStyle={outerStyle}
        ref={ref}
        sx={sharedSx}
        style={{
          ...passthroughStyle,
          ...sharedStyle,
        }}
      >
        <StyledMotiView
          {...(motiProps as any)}
          animate={animate}
          exit={exit}
          from={from}
          style={{ ...bottomStyle, ...sharedStyle, ...baseStyle, ...innerStyle }}
          sx={{ ...sharedSx, ...baseSx, ...innerSx, px: 3 }}
        >
          {renderChildren()}
        </StyledMotiView>
      </MotiPressable>
    );
  }

  const outerStyle = {
    ...calcSx(defaultOuterStyle || {}, { themeKey }),
    ...calcSx(baseSx || {}, { themeKey }),
    ...calcSx(motiPressableOuterSx || {}, { themeKey }),
    ...calcSx(sharedSx || {}, { themeKey }),
    ...baseStyle,
    ...motiPressableOuterStyle,
    ...sharedStyle,
    ...platformStyle,
  };
  return (
    <StyledMotiPressable
      {...(baseProps as any)}
      containerStyle={outerStyle}
      animate={animate}
      exit={exit}
      from={from}
      ref={ref}
      style={{ ...bottomStyle, ...sharedStyle, ...innerStyle }}
      sx={{ ...sharedSx, ...innerSx }}
    >
      {renderChildren()}
    </StyledMotiPressable>
  );
});

export type ButtonProps = PropsWithRef<Props> & RefAttributes<View>;
