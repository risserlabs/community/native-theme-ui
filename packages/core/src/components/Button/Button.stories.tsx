/**
 * File: /src/components/Button/Button.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 17-06-2022 07:34:26
 * Author: Clay Risser
 * -----
 * Last Modified: 05-09-2022 06:59:12
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button } from './index';
import { Text } from '../Text';
import { Box } from '../Box';
import { Args, createArgsStory, createSxArgs, sxArgTypes, autoContrastArgType } from '../../../storybook';

export default {
  title: 'components/buttons/Button',
  component: Button,
  parameters: {
    status: { type: 'beta' },
  },
};

export const button = createArgsStory(Button, {
  onHoverIn: action('onHoverIn'),
  onHoverOut: action('onHoverOut'),
  onLongPress: action('onLongPress'),
  onPress: action('onPress'),
  onPressIn: action('onPressIn'),
  onPressOut: action('onPressOut'),
});
button.args = {
  children: 'Click Me',
  autoContrast: undefined,
  ...createSxArgs(),
};
button.argTypes = {
  hidden: { control: { type: 'boolean' } },
  disabled: { control: { type: 'boolean' } },
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const likeThemUi = () => (
  <Box sx={{ flexDirection: 'row' }}>
    <Button sx={{ mr: 2 }} onPress={action('beep')}>
      Beep
    </Button>
    <Button sx={{ mr: 2 }} onPress={action('boop')}>
      Boop
    </Button>
    <Button sx={{ mr: 2 }} hidden>
      Hidden
    </Button>
  </Box>
);

export const withAutocontrast = (args: Args) => (
  <Button sx={{ bg: args.background }}>
    <Text autoContrast={args.autoContrast}>hello</Text>
  </Button>
);
withAutocontrast.args = { autoContrast: 'AAA', background: 'black' };
withAutocontrast.argTypes = {
  ...autoContrastArgType,
  background: { control: { type: 'color' } },
};

export const animated = () => (
  <Box
    fromSx={{ backgroundColor: 'background' }}
    animateSx={{ backgroundColor: 'primary' }}
    sx={{
      alignContent: 'center',
      flexWrap: 'wrap',
      height: 460,
      justifyContent: 'center',
      padding: 4,
    }}
    transition={{ type: 'timing', duration: 5000 }}
  >
    <Button
      fromSx={{
        bg: 'muted',
      }}
      animateSx={{
        bg: 'secondary',
      }}
      sx={{
        shadowColor: '#000',
        shadowOpacity: 1,
        shadowRadius: 8,
        elevation: 8,
        shadowOffset: {
          height: 0,
          width: 0,
        },
      }}
      autoContrast="AAA"
      transition={{ type: 'timing', duration: 5000 }}
      onHoverIn={action('onHoverIn')}
      onHoverOut={action('onHoverOut')}
      onPress={action('onPress')}
      onPressIn={action('onPressIn')}
      onPressOut={action('onPressInOut')}
    >
      Click Me!
    </Button>
  </Box>
);
