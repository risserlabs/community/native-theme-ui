/**
 * File: /src/components/Heading/H4.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:30:05
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ComponentProps } from 'react';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { AutoContrast } from '@native-theme-ui/auto-contrast';
import { Sx, styled } from '@native-theme-ui/dripsy';
import { em } from '@expo/html-elements/build/css/units';
import { Text } from '../Text';

const themeKey = 'text';

const defaultStyle: Sx = {
  fontSize: em(1),
  fontWeight: '700',
  my: em(1.33),
};

const StyledH4 = styled(Text, {
  themeKey,
  defaultVariant: 'h4',
})(defaultStyle);

export type H4Props = ComponentProps<typeof StyledH4> & {
  autoContrast?: AutoContrast;
};

export const H4: FC<H4Props> = (props: H4Props) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  return <StyledH4 {...props} sx={sx} />;
};
