/**
 * File: /src/components/Heading/Heading.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:54:16
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { H1, H2, H3, H4, H5, H6 } from 'dripsy';
import { Heading } from './index';
import { Args, autoContrastArgType, createArgsStory, createSxArgs, sxArgTypes } from '../../../storybook';

export default {
  title: 'components/text/Heading',
  component: Heading,
  parameters: {
    status: { type: 'beta' },
  },
};

export const heading = (args: Args) => {
  args.as = lookupHeading(args.as);
  return createArgsStory(Heading)(args);
};
heading.args = {
  children: 'Heading!',
  autoContrast: undefined,
  as: 'H1',
  ...createSxArgs(),
};
heading.argTypes = {
  as: {
    control: { type: 'select' },
    options: ['H1', 'H2', 'H3', 'H4', 'H5', 'H6'],
  },
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const h6 = () => <H6>Hello, world!</H6>;

function lookupHeading(type = 'h1') {
  switch (type) {
    case 'H2':
      // @ts-ignore
      return H2;
    case 'H3':
      return H3;
    case 'H4':
      return H4;
    case 'H5':
      return H5;
    case 'H6':
      return H6;
  }
  return H1;
}
