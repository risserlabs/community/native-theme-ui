/**
 * File: /src/components/Heading/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 19-08-2022 08:29:43
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:45:31
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, ComponentProps } from 'react';
import { AutoContrast, useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Sx, styled } from '@native-theme-ui/dripsy';
import { H1, H1Props } from './H1';

const themeKey = 'text';

const defaultStyle: Sx = {
  fontFamily: 'heading',
  fontWeight: 'heading',
};

const StyledHeading = styled(H1, {
  themeKey,
  defaultVariant: 'heading',
})(defaultStyle);

export type HeadingProps = ComponentProps<typeof StyledHeading> & {
  autoContrast?: AutoContrast;
};

export const Heading: FC<HeadingProps> = (props: HeadingProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  return <StyledHeading {...props} sx={sx} />;
};

export { H1 };
export type { H1Props };

export * from './H2';
export * from './H3';
export * from './H4';
export * from './H5';
export * from './H6';
