/**
 * File: /src/components/Heading/H6.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 11:30:29
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Sx, styled } from '@native-theme-ui/dripsy';
import { em } from '@expo/html-elements/build/css/units';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Text, TextProps } from '../Text';

const themeKey = 'text';

const defaultStyle: Sx = {
  fontSize: em(0.67),
  fontWeight: 'bold',
  my: em(2.33),
};

const StyledH6 = styled(Text, {
  themeKey,
  defaultVariant: 'h6',
})(defaultStyle);

export type H6Props = TextProps;

export const H6: FC<H6Props> = (props: H6Props) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return <StyledH6 {...(props as any)} sx={sx} />;
};
