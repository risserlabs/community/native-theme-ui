/**
 * File: /src/components/Avatar/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 19-08-2022 08:29:43
 * Author: Clay Risser
 * -----
 * Last Modified: 04-01-2023 02:14:50
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Sx, styled, useCalculatedSx } from '@native-theme-ui/dripsy';
import { Image, ImageProps } from '../Image';

const defaultStyle: Sx = {
  bg: 'primary',
  borderRadius: 9999,
  objectFit: 'cover',
};

const ThemedImage = styled(Image, {
  defaultVariant: 'avatar',
  themeKey: 'images',
})(defaultStyle);

export type AvatarProps = ImageProps & { size?: number };

export const Avatar: FC<AvatarProps> = (props: AvatarProps) => {
  const sx = useCalculatedSx(props.sx);
  return (
    <ThemedImage
      {...(props as any)}
      sx={{
        width: props.size,
        height: props.size,
        ...sx,
      }}
    />
  );
};

Avatar.defaultProps = {
  size: 48,
};
