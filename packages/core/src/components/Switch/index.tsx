/**
 * File: /src/components/Switch/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 17-06-2022 01:05:11
 * Author: Lavanya Katari
 * -----
 * Last Modified: 27-12-2022 11:03:37
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC, useCallback, useState, useEffect } from 'react';
import { Easing } from 'react-native-reanimated';
import { Platform, StyleSheet } from 'react-native';
import { Sx, styled } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Box, BoxProps } from '../Box';
import { Label } from '../Label';
import { useSplitProps, SwitchProps } from './props';

const themeKey = 'forms';

const StyledOuterBox = styled(Box, {
  themeKey,
  defaultVariant: 'switch',
})(({ showCursor }: { showCursor: boolean }) => ({
  ...Platform.select({
    web: {
      ...(showCursor ? { cursor: 'pointer' } : {}),
    },
  }),
  alignItems: 'center',
}));

const StyledGutterBox = styled(Box, {
  themeKey,
  defaultVariant: 'switch',
})({});

export type StyledBoxProps = BoxProps;

export const Switch: FC<SwitchProps> = (props: SwitchProps) => {
  const [checked, setChecked] = useState(props.defaultChecked ?? false);
  const sx = useAutoContrast(props, {}, themeKey);
  const style = StyleSheet.flatten(props.style || {}) as Sx;
  const { baseProps, checkboxProps, innerSx, innerStyle, baseStyle, baseSx } = useSplitProps(props, sx);
  const bg = (style.backgroundColor || sx.backgroundColor || sx.bg || 'primary') as string;
  let from = {
    transform: [{ translateX: props.checked ?? checked ? props.size! : 0 }],
  };

  useEffect(() => {
    from = {
      transform: [
        {
          translateX: props.checked ?? checked ? 0 : props.size!,
        },
      ],
    };
  }, [props.size, props.checked, checked]);

  const handlePress = useCallback(() => {
    if (props.disabled) return;
    setChecked((checked: boolean) => !checked);
    if (props.onPress) props.onPress();
  }, [props.disabled]);

  return (
    <StyledOuterBox
      {...(baseProps as any)}
      sx={baseSx}
      style={{ ...baseStyle, flexDirection: 'row' }}
      showCursor={!!(props.accessibilityRole === 'link' || !props.disabled)}
      onPress={handlePress}
    >
      {Platform.OS === 'web' && (
        <input
          {...checkboxProps}
          checked={props.checked ?? checked}
          readOnly
          type="checkbox"
          style={{
            height: 1,
            opacity: 0,
            overflow: 'hidden',
            position: 'absolute',
            width: 1,
            zIndex: -1,
          }}
        />
      )}
      <StyledGutterBox
        sx={{
          ...innerSx,
          ...(props.label ? { mr: 2 } : {}),
          bg: props.checked ?? checked ? bg : 'gray',
        }}
        style={{
          ...innerStyle,
          alignItems: 'center',
          borderRadius: props.size!,
          flexDirection: 'row',
          flexShrink: 0,
          height: props.size! + props.gutter! * 2,
          padding: props.gutter!,
          width: props.size! * 2 + props.gutter! * 2,
        }}
        transition={{
          duration: 0,
          type: 'timing',
        }}
      >
        <Box
          from={from}
          animate={{
            transform: [{ translateX: props.checked ?? checked ? props.size! : 0 }],
          }}
          sx={{
            bg: 'white',
            boxShadow: '0 1px 2px rgba(0, 0, 0, 0.1)',
            cursor: 'pointer',
          }}
          style={{
            borderRadius: 9999,
            height: props.size!,
            width: props.size!,
          }}
          transition={{
            duration: 240,
            easing: Easing.bezier(0.165, 0.84, 0.44, 1),
            type: 'timing',
          }}
        />
      </StyledGutterBox>
      {props.label && <Label>{props.label}</Label>}
    </StyledOuterBox>
  );
};

Switch.defaultProps = {
  gutter: 2,
  size: 18,
};

export type { SwitchProps };
