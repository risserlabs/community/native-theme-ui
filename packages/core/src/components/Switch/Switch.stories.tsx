/**
 * File: /src/components/Switch/Switch.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 15-06-2022 06:37:48
 * Author: Lavanya Katari
 * -----
 * Last Modified: 09-09-2022 14:34:56
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { action } from '@storybook/addon-actions';
import { Args, createArgsStory, createSxArgs, sxArgTypes, autoContrastArgType } from '../../../storybook';
import { Box } from '../Box';
import { Switch } from './index';

export default {
  title: 'components/forms/switch',
  component: Switch,
  parameters: {
    status: { type: 'beta' },
  },
};

export const switchCheckbox = createArgsStory(Switch, {
  onChange: action('onChange'),
  onValueChange: action('onValueChange'),
});
switchCheckbox.args = {
  autoContrast: undefined,
  checked: undefined,
  children: undefined,
  defaultChecked: true,
  disabled: false,
  gutter: Switch.defaultProps?.gutter,
  label: 'switch me',
  size: Switch.defaultProps?.size,
  ...createSxArgs(),
};
switchCheckbox.argTypes = {
  checked: { control: { type: 'boolean' } },
  children: { control: { type: 'text' } },
  defaultChecked: { control: { type: 'boolean' } },
  disabled: { control: { type: 'boolean' } },
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const withBackground = (args: Args) => (
  <Box
    sx={{
      padding: 2,
      bg: args.background,
    }}
  >
    <Switch />
  </Box>
);

export const likeThemeUi = () => (
  <Box>
    <Switch defaultChecked />
  </Box>
);
