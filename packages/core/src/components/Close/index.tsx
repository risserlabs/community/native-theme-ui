/**
 * File: /src/components/Close/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:30:52
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { FC } from 'react';
import { Svg, Path } from 'react-native-svg';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { IconButton, IconButtonProps } from '../IconButton';

const closeIcon = ({ color = '#000000', size = 24 }) => (
  <Svg width={size} height={size} fill="currentColor" viewBox="0 0 24 24" color={color}>
    <Path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
  </Svg>
);

export type CloseProps = Omit<IconButtonProps, 'children'>;

export const Close: FC<CloseProps> = (props: CloseProps) => {
  const sx = useAutoContrast(props, {}, 'buttons');
  return (
    <IconButton {...props} sx={sx}>
      {closeIcon({ color: sx.color as string, size: props.size })}
    </IconButton>
  );
};
