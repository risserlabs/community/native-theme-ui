/**
 * File: /src/components/NavLink/NavLink.stories.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 30-08-2022 07:49:16
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { Box } from '../Box';
import { createArgsStory, createSxArgs, sxArgTypes, Args, autoContrastArgType } from '../../../storybook';
import { NavLink } from './index';
import { Flex } from '../Flex';

export default {
  title: 'components/links/NavLink',
  component: NavLink,
  parameters: {
    status: { type: 'beta' },
  },
};

export const navLink = createArgsStory(NavLink);
navLink.args = {
  children: 'I am a NavLink',
  href: 'https://theme-ui.com/components/nav-link',
  autoContrast: NavLink.defaultProps?.autoContrast,
  ...createSxArgs(),
};
navLink.argTypes = {
  ...autoContrastArgType,
  ...sxArgTypes,
};

export const withBackground = (args: Args) => (
  <Box sx={{ bg: args.background, p: 4 }}>
    <NavLink autoContrast={args.autoContrast} href="https://theme-ui.com/components/nav-link">
      Hello, world!
    </NavLink>
  </Box>
);
withBackground.args = {
  autoContrast: 'AAA',
  background: 'black',
};
withBackground.argTypes = {
  ...autoContrastArgType,
  background: { control: { type: 'color' } },
};

export const likeThemeUi = () => (
  <Flex sx={{ width: '100%' }}>
    <NavLink href="#!" sx={{ p: 2 }}>
      Home
    </NavLink>
    <NavLink href="#!" sx={{ p: 2 }}>
      Blog
    </NavLink>
    <NavLink href="#!" sx={{ p: 2 }}>
      About
    </NavLink>
  </Flex>
);
