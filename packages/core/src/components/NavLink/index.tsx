/**
 * File: /src/components/NavLink/index.tsx
 * Project: @native-theme-ui/core
 * File Created: 13-06-2022 00:51:44
 * Author: Clay Risser
 * -----
 * Last Modified: 27-12-2022 12:36:47
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, { ComponentProps, FC, useCallback } from 'react';
import { StyleSheet, TextStyle } from 'react-native';
import { styled, Sx } from '@native-theme-ui/dripsy';
import { useAutoContrast } from '@native-theme-ui/auto-contrast';
import { Link, LinkProps } from '../Link';
import { Box } from '../Box';
import { Text } from '../Text';
import { useAnimationStateSx } from '../../hooks';

const themeKey = 'links';

const defaultOuterStyle: Sx = {
  color: 'background',
  display: 'flex',
  fontWeight: '700',
  width: '100%',
};

const defaultStyle = {
  color: 'background',
  display: 'flex',
  fontWeight: '700',
  textDecoration: 'none',
  width: '100%',
};

const StyledLink = styled(Link, {
  themeKey,
  defaultVariant: 'nav',
})(defaultStyle);

const StyledText = styled(Text, {
  themeKey,
  defaultVariant: 'nav',
})(defaultStyle);

const StyledOuterBox = styled(Box, {
  themeKey,
  defaultVariant: 'nav',
})(defaultOuterStyle);

type StyledTextProps = ComponentProps<typeof StyledText>;

export type NavLinkProps = LinkProps;

export const NavLink: FC<NavLinkProps> = (props: NavLinkProps) => {
  const sx = useAutoContrast(props, defaultStyle, themeKey);
  const style = StyleSheet.flatten([{ borderRadius: 2 }, StyleSheet.flatten(props.style || {})]) as TextStyle;
  const animationState = useAnimationStateSx({
    from: {},
    to: {
      backgroundColor: 'transparent',
    },
    hover: {
      backgroundColor: style.backgroundColor || sx.backgroundColor || sx.bg || 'highlight',
    },
  });

  const handleEnter = useCallback(() => {
    animationState.transitionTo('hover');
  }, []);

  const handleLeave = useCallback(() => {
    animationState.transitionTo('to');
  }, []);

  function renderLink() {
    if (props.href === '#!') {
      const textProps = { ...props };
      delete textProps.href;
      return (
        <StyledText
          {...(props as StyledTextProps)}
          state={animationState}
          style={style}
          sx={sx}
          transition={{
            type: 'timing',
            duration: 200,
          }}
        />
      );
    }
    return <StyledLink {...props} sx={sx} />;
  }

  return (
    <StyledOuterBox onHoverIn={handleEnter} onHoverOut={handleLeave} onPressIn={handleEnter} onPressOut={handleLeave}>
      {renderLink()}
    </StyledOuterBox>
  );
};
